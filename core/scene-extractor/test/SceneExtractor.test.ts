import * as path from 'path';
import {
  DemoScenes, KillByWeaponAndPlayerSelectorParams, SceneExtractor, SceneSelectorConfig,
} from '../src';
import { XKillsPerRoundSelectorParams } from '../src/scene-selectors/XKillsPerRoundSelector';

const demoFile = `${__dirname}${path.sep}..${path.sep}..${path.sep}..${path.sep}test-resources${path.sep}strike.dem`;

describe('SceneExtractor', () => {
  it('can be instanced', () => {
    expect(() => new SceneExtractor()).not.toThrow();
  });

  describe('extractScenes', () => {
    it('throws on empty SceneSelectorConfig', () => {
      const sceneExtractor = new SceneExtractor();
      expect(() => sceneExtractor.extractScenes(demoFile, [])).rejects.toThrow(new Error('Empty Scene Selector Config.'));
    });

    it('throws on unknown SceneSelector', () => {
      const sceneExtractor = new SceneExtractor();
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      expect(() => sceneExtractor.extractScenes(demoFile, [{ name: 'unknown' }])).rejects.toThrow(new Error('Unknown SceneSelector unknown'));
    });

    it('throws on invalid demo path', () => {
      const sceneExtractor = new SceneExtractor();

      const steam64Id = '76561198141200296';
      const weapon = 'deagle';
      const introSeconds = 5;
      const outroSeconds = 5;
      const DEATH_EVENT_SECONDS = 0;

      const params: KillByWeaponAndPlayerSelectorParams = {
        weapon, steam64Id, eventLength: DEATH_EVENT_SECONDS, introSeconds, outroSeconds,
      };

      const sceneSelectorConfig: SceneSelectorConfig[] = [
        { name: 'KillByWeaponAndPlayerSelector', ...params },
      ];
      expect(() => sceneExtractor.extractScenes(`wrong${path.sep}path.css`, sceneSelectorConfig)).rejects.toThrow(new Error(`Demo File "wrong${path.sep}path.css" not found!`));
    });

    it('extracts Scenes', () => {
      const sceneExtractor = new SceneExtractor();

      const steam64Id = '76561198157658985';
      const weapon = 'glock';
      const introSeconds = 5;
      const outroSeconds = 5;
      const eventLength = 0;

      const params: KillByWeaponAndPlayerSelectorParams = {
        weapon, steam64Id, eventLength, introSeconds, outroSeconds,
      };

      const sceneSelectorConfig: SceneSelectorConfig[] = [
        { name: 'KillByWeaponAndPlayerSelector', ...params },
      ];

      jest.setTimeout(10);
      return sceneExtractor.extractScenes(demoFile, sceneSelectorConfig).then((scenes: DemoScenes) => {
        expect(scenes.file).toEqual(demoFile);
        expect(scenes.tickRate).toEqual(64);
        expect(scenes.scenes.length).toEqual(2);
      });
    });

    it('includes warmup', () => {
      const sceneExtractor = new SceneExtractor();

      const steam64Id = '76561198157658985';
      const weapon = 'glock';
      const introSeconds = 5;
      const outroSeconds = 5;
      const eventLength = 0;

      const params: KillByWeaponAndPlayerSelectorParams = {
        weapon, steam64Id, eventLength, introSeconds, outroSeconds,
      };

      const sceneSelectorConfig: SceneSelectorConfig[] = [
        { name: 'KillByWeaponAndPlayerSelector', ...params },
      ];

      jest.setTimeout(10);
      return sceneExtractor.extractScenes(demoFile, sceneSelectorConfig, undefined, true).then((scenes: DemoScenes) => {
        expect(scenes.scenes.length).toEqual(4);
      });
    });
  });

  it('checks Scene Conditions', () => {
    const sceneExtractor = new SceneExtractor();

    const introSeconds = 5;
    const outroSeconds = 5;
    const eventLength = 0;

    const params: XKillsPerRoundSelectorParams = {
      killTarget: 2, eventLength, introSeconds, outroSeconds,
    };

    const sceneSelectorConfig: SceneSelectorConfig[] = [
      { name: 'XKillsPerRoundSelector', ...params },
    ];

    jest.setTimeout(10);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore todo investigate
    const spy = jest.spyOn(sceneExtractor, 'checkConditions');
    return sceneExtractor.extractScenes(demoFile, sceneSelectorConfig).then(() => {
      expect(spy).toHaveBeenCalled();
    });
  });
});

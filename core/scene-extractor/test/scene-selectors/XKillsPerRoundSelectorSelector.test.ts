import * as path from 'path';
import {
  XKillsPerRoundSelectorParams,
  DemoScenes, SceneExtractor, SceneSelectorConfig, XKillsPerRoundSelector,
} from '../../src';

const demoFile = `${__dirname}${path.sep}..${path.sep}..${path.sep}..${path.sep}..${path.sep}test-resources${path.sep}strike.dem`;

describe('XKillsPerRoundSelector', () => {
  it('can be instanced', () => {
    const params: XKillsPerRoundSelectorParams = {
      killTarget: 2,
      eventLength: 0.5,
      introSeconds: 3,
      outroSeconds: 4,
    };

    expect(() => new XKillsPerRoundSelector(params)).not.toThrow();
  });

  it('throws on non positive kill target', () => {
    const failingParams: XKillsPerRoundSelectorParams = {
      killTarget: 0,
      eventLength: 0.5,
      introSeconds: 3,
      outroSeconds: 4,
    };

    expect(() => new XKillsPerRoundSelector(failingParams)).toThrow(new Error('killTarget must be positive!'));
  });

  // todo check that scene start is actually rounds start
  it('selects double kill by player', () => {
    const params: XKillsPerRoundSelectorParams = {
      killTarget: 2,
      eventLength: 0.5,
      introSeconds: 3,
      outroSeconds: 4,
    };

    const sceneSelectorConfig: SceneSelectorConfig[] = [{ name: 'XKillsPerRoundSelector', ...params }];
    const sceneExtractor = new SceneExtractor();

    return sceneExtractor.extractScenes(demoFile, sceneSelectorConfig).then((demoScenes: DemoScenes) => {
      expect(demoScenes.scenes.length).toEqual(1);
    });
  });

  it('selects double kill by different player', () => {
    const params: XKillsPerRoundSelectorParams = {
      killTarget: 2,
      eventLength: 0.5,
      introSeconds: 3,
      outroSeconds: 4,
    };
    const sceneSelectorConfig: SceneSelectorConfig[] = [{ name: 'XKillsPerRoundSelector', ...params }];
    const sceneExtractor = new SceneExtractor();

    return sceneExtractor.extractScenes(demoFile, sceneSelectorConfig).then((demoScenes: DemoScenes) => {
      expect(demoScenes.scenes.length).toEqual(1);
    });
  });

  xit('selects triple kill by player', () => { // todo why is this failing? maybe round_end event is not fired but match_end or something like this -> add the ability to listen for multiple events
    const params: XKillsPerRoundSelectorParams = {
      killTarget: 3,
      eventLength: 0.5,
      introSeconds: 3,
      outroSeconds: 4,
    };
    const sceneSelectorConfig: SceneSelectorConfig[] = [{ name: 'XKillsPerRoundSelector', ...params }];
    const sceneExtractor = new SceneExtractor();

    return sceneExtractor.extractScenes(demoFile, sceneSelectorConfig).then((demoScenes: DemoScenes) => {
      expect(demoScenes.scenes.length).toEqual(1);
    });
  });
});

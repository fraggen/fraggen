import * as path from 'path';
import {
  DemoScenes, KillByWeaponAndPlayerSelector, KillByWeaponAndPlayerSelectorParams, SceneExtractor, SceneSelectorConfig,
} from '../../src';

const demoFile = `${__dirname}${path.sep}..${path.sep}..${path.sep}..${path.sep}..${path.sep}test-resources${path.sep}strike.dem`;

describe('KillByAndPlayerSelector', () => {
  it('can be instanced', () => {
    const params: KillByWeaponAndPlayerSelectorParams = {
      steam64Id: '76561198127226178',
      weapon: 'usp_silencer',
      eventLength: 0.5,
      introSeconds: 3,
      outroSeconds: 4,
    };

    expect(() => new KillByWeaponAndPlayerSelector(params)).not.toThrow();
  });

  it('selects all kills with usp by specific player', () => {
    const params: KillByWeaponAndPlayerSelectorParams = {
      steam64Id: '76561198070717110',
      weapon: 'usp_silencer',
      eventLength: 0.5,
      introSeconds: 3,
      outroSeconds: 4,
    };

    const sceneSelectorConfig: SceneSelectorConfig[] = [{ name: 'KillByWeaponAndPlayerSelector', ...params }];
    const sceneExtractor = new SceneExtractor();

    return sceneExtractor.extractScenes(demoFile, sceneSelectorConfig).then((demoScenes: DemoScenes) => {
      expect(demoScenes.scenes.length).toEqual(1);
    });
  });

  it('does not select kills from wrong player', () => {
    const params: KillByWeaponAndPlayerSelectorParams = {
      steam64Id: '76561198052982918',
      weapon: 'usp_silencer',
      eventLength: 0.5,
      introSeconds: 3,
      outroSeconds: 4,
    };

    const sceneSelectorConfig: SceneSelectorConfig[] = [{ name: 'KillByWeaponAndPlayerSelector', ...params }];
    const sceneExtractor = new SceneExtractor();

    return sceneExtractor.extractScenes(demoFile, sceneSelectorConfig).then((demoScenes: DemoScenes) => {
      expect(demoScenes.scenes.length).toEqual(0);
    });
  });

  it('it does not select kills with wrong weapon', () => {
    const params: KillByWeaponAndPlayerSelectorParams = {
      steam64Id: '76561198070717110',
      weapon: 'glock',
      eventLength: 0.5,
      introSeconds: 3,
      outroSeconds: 4,
    };

    const sceneSelectorConfig: SceneSelectorConfig[] = [{ name: 'KillByWeaponAndPlayerSelector', ...params }];
    const sceneExtractor = new SceneExtractor();

    return sceneExtractor.extractScenes(demoFile, sceneSelectorConfig).then((demoScenes: DemoScenes) => {
      expect(demoScenes.scenes.length).toEqual(0);
    });
  });
});

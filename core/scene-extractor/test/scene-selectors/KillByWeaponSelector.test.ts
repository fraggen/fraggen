import * as path from 'path';
import {
  DemoScenes, KillByWeaponSelector, KillByWeaponSelectorParams, SceneExtractor, SceneSelectorConfig,
} from '../../src';

const demoFile = `${__dirname}${path.sep}..${path.sep}..${path.sep}..${path.sep}..${path.sep}test-resources${path.sep}strike.dem`;

describe('KillByWeaponSelector', () => {
  it('can be instanced', () => {
    const params: KillByWeaponSelectorParams = {
      weapon: 'usp_silencer',
      eventLength: 0.5,
      introSeconds: 3,
      outroSeconds: 4,
    };

    expect(() => new KillByWeaponSelector(params)).not.toThrow();
  });

  it('selects all kills with usp_silencer', () => {
    const params: KillByWeaponSelectorParams = {
      weapon: 'usp_silencer',
      eventLength: 0.5,
      introSeconds: 3,
      outroSeconds: 4,
    };

    const sceneSelectorConfig: SceneSelectorConfig[] = [{ name: 'KillByWeaponSelector', ...params }];
    const sceneExtractor = new SceneExtractor();

    return sceneExtractor.extractScenes(demoFile, sceneSelectorConfig).then((demoScenes: DemoScenes) => {
      expect(demoScenes.scenes.length).toEqual(1);
    });
  });

  it('selects all kills with glock', () => {
    const params: KillByWeaponSelectorParams = {
      weapon: 'glock',
      eventLength: 0.5,
      introSeconds: 3,
      outroSeconds: 4,
    };

    const sceneSelectorConfig: SceneSelectorConfig[] = [{ name: 'KillByWeaponSelector', ...params }];
    const sceneExtractor = new SceneExtractor();

    return sceneExtractor.extractScenes(demoFile, sceneSelectorConfig).then((demoScenes: DemoScenes) => {
      expect(demoScenes.scenes.length).toEqual(3);
    });
  });
});

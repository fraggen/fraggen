import * as path from 'path';
import {
  DemoScenes, SceneExtractor, SceneSelectorConfig,
  XKillsPerRoundByPlayerSelector, XKillsPerRoundByPlayerSelectorSelectorParams,
} from '../../src';

const demoFile = `${__dirname}${path.sep}..${path.sep}..${path.sep}..${path.sep}..${path.sep}test-resources${path.sep}strike.dem`;

describe('XKillsPerRoundByPlayerSelector', () => {
  it('can be instanced', () => {
    const params: XKillsPerRoundByPlayerSelectorSelectorParams = {
      killTarget: 2,
      steam64Id: '76561198157658985',
      eventLength: 0.5,
      introSeconds: 3,
      outroSeconds: 4,
    };

    expect(() => new XKillsPerRoundByPlayerSelector(params)).not.toThrow();
  });

  it('throws on non positive kill target', () => {
    const failingParams: XKillsPerRoundByPlayerSelectorSelectorParams = {
      killTarget: 0,
      steam64Id: '76561198157658985',
      eventLength: 0.5,
      introSeconds: 3,
      outroSeconds: 4,
    };

    expect(() => new XKillsPerRoundByPlayerSelector(failingParams)).toThrow(new Error('killTarget must be positive!'));
  });

  it('selects double kill by player', () => {
    const params: XKillsPerRoundByPlayerSelectorSelectorParams = {
      killTarget: 2,
      steam64Id: '76561198157658985',
      eventLength: 0.5,
      introSeconds: 3,
      outroSeconds: 4,
    };

    const sceneSelectorConfig: SceneSelectorConfig[] = [{ name: 'XKillsPerRoundByPlayerSelector', ...params }];
    const sceneExtractor = new SceneExtractor();

    return sceneExtractor.extractScenes(demoFile, sceneSelectorConfig).then((demoScenes: DemoScenes) => {
      expect(demoScenes.scenes.length).toEqual(1);
      expect(demoScenes.scenes[0].playerToSpec).toEqual('76561198157658985');
    });
  });

  it('selects double kill by different player', () => {
    const params: XKillsPerRoundByPlayerSelectorSelectorParams = {
      killTarget: 2,
      steam64Id: '76561198165430042',
      eventLength: 0.5,
      introSeconds: 3,
      outroSeconds: 4,
    };
    const sceneSelectorConfig: SceneSelectorConfig[] = [{ name: 'XKillsPerRoundByPlayerSelector', ...params }];
    const sceneExtractor = new SceneExtractor();

    return sceneExtractor.extractScenes(demoFile, sceneSelectorConfig).then((demoScenes: DemoScenes) => {
      expect(demoScenes.scenes.length).toEqual(1);
      expect(demoScenes.scenes[0].playerToSpec).toEqual('76561198165430042');
    });
  });

  xit('selects triple kill by player', () => { // todo why is this failing? maybe round_end event is not fired but match_end or something like this -> add the ability to listen for multiple events
    const params: XKillsPerRoundByPlayerSelectorSelectorParams = {
      killTarget: 3,
      steam64Id: '76561198127226178',
      eventLength: 0.5,
      introSeconds: 3,
      outroSeconds: 4,
    };
    const sceneSelectorConfig: SceneSelectorConfig[] = [{ name: 'XKillsPerRoundByPlayerSelector', ...params }];
    const sceneExtractor = new SceneExtractor();

    return sceneExtractor.extractScenes(demoFile, sceneSelectorConfig).then((demoScenes: DemoScenes) => {
      expect(demoScenes.scenes.length).toEqual(1);
      expect(demoScenes.scenes[0].playerToSpec).toEqual('76561198127226178');
    });
  });
});

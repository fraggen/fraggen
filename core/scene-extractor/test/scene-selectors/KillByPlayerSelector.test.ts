import * as path from 'path';
import {
  DemoScenes, KillByPlayerSelector, KillByPlayerSelectorParams, SceneExtractor, SceneSelectorConfig,
} from '../../src';

const demoFile = `${__dirname}${path.sep}..${path.sep}..${path.sep}..${path.sep}..${path.sep}test-resources${path.sep}strike.dem`;

describe('KillByPlayerSelector', () => {
  it('can be instanced', () => {
    const params: KillByPlayerSelectorParams = {
      steam64Id: '76561198127226178',
      eventLength: 0.5,
      introSeconds: 3,
      outroSeconds: 4,
    };

    expect(() => new KillByPlayerSelector(params)).not.toThrow();
  });

  it('selects all kills from a specific player', () => {
    const params: KillByPlayerSelectorParams = {
      steam64Id: '76561198127226178',
      eventLength: 0.5,
      introSeconds: 3,
      outroSeconds: 4,
    };

    const sceneSelectorConfig: SceneSelectorConfig[] = [{ name: 'KillByPlayerSelector', ...params }];
    const sceneExtractor = new SceneExtractor();

    return sceneExtractor.extractScenes(demoFile, sceneSelectorConfig).then((demoScenes: DemoScenes) => {
      expect(demoScenes.scenes.length).toEqual(3);
    });
  });
});

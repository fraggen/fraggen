import * as path from 'path';
import {
  DemoScenes, SceneExtractor, SceneSelectorConfig, ZoomInAndDelayAndKillSelector, ZoomInAndDelayAndKillSelectorParams,
} from '../../src';

const demoFile = `${__dirname}${path.sep}..${path.sep}..${path.sep}..${path.sep}..${path.sep}test-resources${path.sep}strike.dem`;

describe('ZoomInAndDelayAndKillSelector', () => {
  const params: ZoomInAndDelayAndKillSelectorParams = {
    delay: 0.1,
    killTimeoutSeconds: 0.5,
    eventLength: 0.5,
    introSeconds: 3,
    outroSeconds: 4,
  };

  it('can be instanced', () => {
    expect(() => new ZoomInAndDelayAndKillSelector(params)).not.toThrow();
  });

  it('throws when killTimeout >= outroSeconds', () => {
    const failingParams: ZoomInAndDelayAndKillSelectorParams = {
      delay: 0.1,
      killTimeoutSeconds: 4,
      eventLength: 0.5,
      introSeconds: 3,
      outroSeconds: 4,
    };

    expect(() => new ZoomInAndDelayAndKillSelector(failingParams)).toThrow(new Error('killTimeout must be smaller than outroSeconds!'));
  });

  it('selects zoom in events', () => {
    const sceneSelectorConfig: SceneSelectorConfig[] = [{ name: 'ZoomInAndDelayAndKillSelector', ...params }];
    const sceneExtractor = new SceneExtractor();

    return sceneExtractor.extractScenes(demoFile, sceneSelectorConfig, undefined, true).then((demoScenes: DemoScenes) => {
      expect(demoScenes.scenes.length).toEqual(2); // todo verify lol
    });
  });
});

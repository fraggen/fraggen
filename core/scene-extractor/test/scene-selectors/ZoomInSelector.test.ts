import * as path from 'path';
import {
  DemoScenes, SceneExtractor, SceneSelectorConfig, SceneSelectorParams, ZoomInSelector,
} from '../../src';

const demoFile = `${__dirname}${path.sep}..${path.sep}..${path.sep}..${path.sep}..${path.sep}test-resources${path.sep}strike.dem`;

describe('ZoomInSelector', () => {
  it('can be instanced', () => {
    const params: SceneSelectorParams = {
      eventLength: 0.5,
      introSeconds: 3,
      outroSeconds: 4,
    };

    expect(() => new ZoomInSelector(params)).not.toThrow();
  });

  it('selects zoom in events', () => {
    const params: SceneSelectorParams = {
      eventLength: 0.5,
      introSeconds: 3,
      outroSeconds: 4,
    };

    const sceneSelectorConfig: SceneSelectorConfig[] = [{ name: 'ZoomInSelector', ...params }];
    const sceneExtractor = new SceneExtractor();

    return sceneExtractor.extractScenes(demoFile, sceneSelectorConfig, undefined, true).then((demoScenes: DemoScenes) => {
      expect(demoScenes.scenes.length).toEqual(14); // todo verify 14 zoom in events in warmup
    });
  });
});

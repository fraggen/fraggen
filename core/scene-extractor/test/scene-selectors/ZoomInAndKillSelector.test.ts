import * as path from 'path';
import {
  DemoScenes, SceneExtractor, SceneSelectorConfig, ZoomInAndKillSelector, ZoomInAndKillSelectorParams,
} from '../../src';

const demoFile = `${__dirname}${path.sep}..${path.sep}..${path.sep}..${path.sep}..${path.sep}test-resources${path.sep}strike.dem`;

describe('ZoomInAndKillSelector', () => {
  const params: ZoomInAndKillSelectorParams = {
    killTimeoutSeconds: 0.5,
    eventLength: 0.5,
    introSeconds: 3,
    outroSeconds: 4,
  };

  it('can be instanced', () => {
    expect(() => new ZoomInAndKillSelector(params)).not.toThrow();
  });

  it('throws when killTimeout >= outroSeconds', () => {
    const failingParams: ZoomInAndKillSelectorParams = {
      killTimeoutSeconds: 4,
      eventLength: 0.5,
      introSeconds: 3,
      outroSeconds: 4,
    };

    expect(() => new ZoomInAndKillSelector(failingParams)).toThrow(new Error('killTimeout must be smaller than outroSeconds!'));
  });

  it('selects zoom in events', () => {
    const sceneSelectorConfig: SceneSelectorConfig[] = [{ name: 'ZoomInAndKillSelector', ...params }];
    const sceneExtractor = new SceneExtractor();

    return sceneExtractor.extractScenes(demoFile, sceneSelectorConfig, undefined, true).then((demoScenes: DemoScenes) => {
      expect(demoScenes.scenes.length).toEqual(2); // todo verify lol
    });
  });
});

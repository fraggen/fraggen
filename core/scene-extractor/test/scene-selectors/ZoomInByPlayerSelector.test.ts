import * as path from 'path';
import {
  DemoScenes, SceneExtractor, SceneSelectorConfig, ZoomInByPlayerSelector, ZoomInByPlayerSelectorParams,
} from '../../src';

const demoFile = `${__dirname}${path.sep}..${path.sep}..${path.sep}..${path.sep}..${path.sep}test-resources${path.sep}strike.dem`;

describe('ZoomInByPlayerSelector', () => {
  const params: ZoomInByPlayerSelectorParams = {
    steam64Id: '76561198070717110',
    eventLength: 0.5,
    introSeconds: 3,
    outroSeconds: 4,
  };

  it('can be instanced', () => {
    expect(() => new ZoomInByPlayerSelector(params)).not.toThrow();
  });

  it('selects zoom in events', () => {
    const sceneSelectorConfig: SceneSelectorConfig[] = [{ name: 'ZoomInByPlayerSelector', ...params }];
    const sceneExtractor = new SceneExtractor();

    return sceneExtractor.extractScenes(demoFile, sceneSelectorConfig, undefined, true).then((demoScenes: DemoScenes) => {
      expect(demoScenes.scenes.length).toEqual(1); // todo verify
    });
  });
});

import { DemoFile } from 'demofile';
import {
  DemoEvent,
} from '../../src';
import { SceneSelector, SceneSelectorParams } from '../../src/scene-selectors/SceneSelector';

describe('SceneSelector', () => {
  it('can be instanced', () => {
    const params: SceneSelectorParams = {
      eventLength: 0.5,
      introSeconds: 3,
      outroSeconds: 4,
    };

    expect(() => new SceneSelector(params)).not.toThrow();
  });

  it('selects a Scene correctly', () => {
    const params: SceneSelectorParams = {
      eventLength: 0.5,
      introSeconds: 3,
      outroSeconds: 4,
    };

    const sceneSelector = new SceneSelector(params);

    expect(sceneSelector.name.length).toBeGreaterThan(0);
    expect(sceneSelector.description.length).toBeGreaterThan(0);
    expect(sceneSelector.conditions.length).toEqual(0);
    expect(sceneSelector.eventName).toEqual(DemoEvent.EVENT);
    expect(params).toEqual(params);

    const fakeDemoFile: DemoFile = { players: [{ steamId64: 'steamid' }], currentTick: 2000, tickRate: 24 } as unknown as DemoFile;
    const testEvent = 'testevent';
    const scenes = sceneSelector.select(sceneSelector, testEvent, fakeDemoFile);

    expect(scenes.length).toEqual(1);

    const scene = scenes[0];
    const expectedLength = params.eventLength + params.introSeconds + params.outroSeconds;
    const expectedStartTick = Math.max(fakeDemoFile.currentTick - Math.floor(fakeDemoFile.tickRate * (params.introSeconds + params.eventLength / 2)), 0);
    const expectedEndTick = fakeDemoFile.currentTick + Math.ceil(fakeDemoFile.tickRate * (params.outroSeconds + params.eventLength / 2));

    expect(scene.length).toEqual(expectedLength);
    expect(scene.startTick).toEqual(expectedStartTick);
    expect(scene.eventTick).toEqual(fakeDemoFile.currentTick);
    expect(scene.endTick).toEqual(expectedEndTick);
  });
});

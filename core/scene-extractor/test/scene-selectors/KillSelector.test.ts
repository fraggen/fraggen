import * as path from 'path';
import {
  DemoScenes, KillSelector, SceneExtractor, SceneSelectorConfig, SceneSelectorParams,
} from '../../src';

const demoFile = `${__dirname}${path.sep}..${path.sep}..${path.sep}..${path.sep}..${path.sep}test-resources${path.sep}strike.dem`;

describe('KillSelector', () => {
  it('can be instanced', () => {
    const params: SceneSelectorParams = {
      eventLength: 0.5,
      introSeconds: 3,
      outroSeconds: 4,
    };

    expect(() => new KillSelector(params)).not.toThrow();
  });

  it('selects all kills', () => {
    const params: SceneSelectorParams = {
      eventLength: 0.5,
      introSeconds: 3,
      outroSeconds: 4,
    };

    const sceneSelectorConfig: SceneSelectorConfig[] = [{ name: 'KillSelector', ...params }];
    const sceneExtractor = new SceneExtractor();

    return sceneExtractor.extractScenes(demoFile, sceneSelectorConfig).then((demoScenes: DemoScenes) => {
      expect(demoScenes.scenes.length).toEqual(11);
    });
  });
});

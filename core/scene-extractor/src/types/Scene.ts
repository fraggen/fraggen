import { SceneSelector } from '../scene-selectors/SceneSelector';

export interface Scene {
    id?: number
    sceneSelector: SceneSelector
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    event: any
    startTick: number
    eventTick: number
    endTick: number
    playerToSpec: string
    length: number
    folder?: string
}

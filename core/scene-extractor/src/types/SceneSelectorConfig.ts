import { SceneSelectorParams } from '../scene-selectors/SceneSelector';

export interface SceneSelectorConfig extends SceneSelectorParams {
  name: 'KillSelector' | 'KillByPlayerSelector'| 'KillByWeaponAndPlayerSelector'| 'KillByWeaponSelector'| 'ZoomInSelector'| 'ZoomInByPlayerSelector'| 'ZoomInAndKillSelector'| 'ZoomInAndDelayAndKillSelector' | 'XKillsPerRoundSelector' | 'XKillsPerRoundByPlayerSelector',
}

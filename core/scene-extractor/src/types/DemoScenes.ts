import { Scene } from './Scene';

export interface DemoScenes {
    file: string
    tickRate: number
    scenes: Scene[]
}

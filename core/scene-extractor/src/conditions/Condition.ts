import { SceneSelector } from '../scene-selectors/SceneSelector';
import { Scene } from '../types/Scene';

export default class Condition {
  name: string;

  delay?: number; // seconds that have to pass after the initial event until the condition may resolve.

  delayFilter?: boolean; // if true the delay acts as a filter. If the condition is resolved before the delay the condition will fail. If false and the condition is resolved before the delay the condition will be re-registered until it is resolved or the timeout applies.

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  sceneSelector: (gameEvent: any, scene: Scene) => SceneSelector;

  constructor(
    name: string,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    sceneSelector: (gameEvent: any, scene: Scene) => SceneSelector,
    delay?: number,
    delayFilter?: boolean,
  ) {
    this.name = name;
    this.delay = delay;
    this.delayFilter = delayFilter;
    this.sceneSelector = sceneSelector;
  }
}

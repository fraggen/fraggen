import { IEventRoundEnd } from 'demofile';
import { SceneSelector } from '../scene-selectors/SceneSelector';
import { Scene } from '../types/Scene';
import Condition from './Condition';

export default class RoundEndCondition extends Condition {
  sceneSelector: (gameEvent: IEventRoundEnd, scene: Scene) => SceneSelector;

  constructor(
    name: string,
    sceneSelector: (gameEvent: IEventRoundEnd, scene: Scene) => SceneSelector,
    delay?: number,
    delayFilter?: boolean,
  ) {
    super(name, sceneSelector, delay, delayFilter);
    this.sceneSelector = sceneSelector;
  }
}

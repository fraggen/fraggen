import { SceneSelector } from '../scene-selectors/SceneSelector';
import Condition from './Condition';

export class TimeoutCondition extends Condition {
  timeout: number; // seconds until the condition fails if it does not resolve. Default value is the initial events' outro length.

  constructor(
    name: string,
    timeout: number,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    sceneSelector: (gameEvent: any) => SceneSelector,
    delay?: number,
    delayFilter?: boolean,
  ) {
    super(name, sceneSelector, delay, delayFilter);
    this.timeout = timeout;
  }
}

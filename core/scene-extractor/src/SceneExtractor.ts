import { DemoFile } from 'demofile';
import * as fs from 'fs';
import { WebSocket, Event } from 'ws';
import Condition from './conditions/Condition';
import MatchEndCondition from './conditions/MatchEndCondition';
import RoundEndCondition from './conditions/RoundEndCondition';
import { TimeoutCondition } from './conditions/TimeoutCondition';
import { KillByPlayerSelector, KillByPlayerSelectorParams } from './scene-selectors/KillByPlayerSelector';
import { KillByWeaponAndPlayerSelector, KillByWeaponAndPlayerSelectorParams } from './scene-selectors/KillByWeaponAndPlayerSelector';
import { KillByWeaponSelector, KillByWeaponSelectorParams } from './scene-selectors/KillByWeaponSelector';
import { KillSelector } from './scene-selectors/KillSelector';
import { SceneSelector } from './scene-selectors/SceneSelector';
import { XKillsPerRoundByPlayerSelector, XKillsPerRoundByPlayerSelectorSelectorParams } from './scene-selectors/XKillsPerRoundByPlayerSelector';
import { XKillsPerRoundSelector, XKillsPerRoundSelectorParams } from './scene-selectors/XKillsPerRoundSelector';
import { ZoomInAndDelayAndKillSelector, ZoomInAndDelayAndKillSelectorParams } from './scene-selectors/ZoomInAndDelayAndKillSelector';
import { ZoomInAndKillSelector, ZoomInAndKillSelectorParams } from './scene-selectors/ZoomInAndKillSelector';
import { ZoomInByPlayerSelector, ZoomInByPlayerSelectorParams } from './scene-selectors/ZoomInByPlayerSelector';
import { ZoomInSelector } from './scene-selectors/ZoomInSelector';
import { DemoEvent } from './types/DemoEvent';
import { DemoScenes } from './types/DemoScenes';
import { Scene } from './types/Scene';
import { SceneSelectorConfig } from './types/SceneSelectorConfig';

// extracts Scenes selected by SceneSelectors from demo files
export class SceneExtractor {
  private demoFile: DemoFile = new DemoFile();

  private scenes: Scene[] = [];

  private logServerConnection: WebSocket | undefined;

  // registers sceneSelectors, replays the demo and collects Scenes to record later
  public async extractScenes(demoPath: string, sceneSelectorConfig: SceneSelectorConfig[], logPort?: number, includeWarmup = false): Promise<DemoScenes> {
    this.initLogServerConnection(logPort);

    if (demoPath.length === 0 || !fs.existsSync(demoPath)) {
      throw new Error(`Demo File "${demoPath}" not found!`);
    }

    this.demoFile = new DemoFile();
    this.demoFile.setMaxListeners(1000);
    this.demoFile.gameEvents.setMaxListeners(1000);
    this.scenes = [];

    const sceneSelectors = this.getSceneSelectors(sceneSelectorConfig);

    return new Promise<DemoScenes>((resolve, reject) => {
      if (includeWarmup) {
        this.registerSceneSelectors(sceneSelectors);
      } else {
        this.demoFile.gameEvents.once(DemoEvent.ROUND_ANNOUNCE_MATCH_START, () => {
          this.registerSceneSelectors(sceneSelectors);
        });
      }

      this.demoFile.on('end', (e) => {
        if (e.error) {
          this.log(`Error during parsing ${demoPath}: ${e.error}`);
          reject(e.error);
        }

        const demoScenes: DemoScenes = { file: demoPath, tickRate: this.demoFile.tickRate, scenes: this.scenes };

        resolve(demoScenes);
      });

      this.demoFile.parseStream(fs.createReadStream(demoPath));
    });
  }

  private registerSceneSelectors(sceneSelectors: SceneSelector[]) {
    sceneSelectors.forEach((sceneSelector: SceneSelector) => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore todo PR in DemoFile - two comments are too much!
      this.demoFile.gameEvents.on(sceneSelector.eventName, (gameEvent: DemoEvent) => {
        const scenes = sceneSelector.select(sceneSelector, gameEvent, this.demoFile);

        if (scenes.length > 0) {
          if (sceneSelector.conditions.length > 0) {
            this.log('checking conditions');
            this.checkConditions(sceneSelector, gameEvent, scenes);
          } else {
            scenes.forEach((scene: Scene) => {
              this.addScene(scene);
            });
          }
        }
      });
    });
  }

  private addScene(scene: Scene) {
    // eslint-disable-next-line no-param-reassign
    scene.id = this.scenes.length + 1;
    this.scenes.push(scene);
    this.log(`Added scene ${scene.id}, found by ${scene.sceneSelector.name}.`);
  }

  private checkConditions(sceneSelector: SceneSelector, gameEvent: DemoEvent, scenes: Scene[]) {
    scenes.forEach((scene: Scene) => {
      sceneSelector.conditions.forEach((condition: Condition) => {
        if (condition instanceof RoundEndCondition) {
          this.checkGameEventCondition(condition, gameEvent, DemoEvent.ROUND_END, scene);
        } else if (condition instanceof MatchEndCondition) {
          this.checkGameEventCondition(condition, gameEvent, DemoEvent.CS_WIN_PANEL_MATCH, scene);
        } else if (condition instanceof TimeoutCondition) {
          this.checkTimeoutCondition(condition, gameEvent, scene);
        }
      });
    });
  }

  private checkGameEventCondition(condition: Condition, sourceGameEvent: DemoEvent, targetGameEvent: DemoEvent, scene: Scene) {
    const conditionSceneSelector = condition.sceneSelector(sourceGameEvent, scene); // initalize the condition with the source game event to pass important params like a steam64Id

    // tick when the condition may resolve
    let delayTick: number | null = null;
    if (condition.delay) {
      delayTick = scene.eventTick + Math.ceil(condition.delay * this.demoFile.tickRate);
    }

    const checkCondition = (conditionGameEvent: DemoEvent) => {
      const conditionScenes = conditionSceneSelector.select(conditionSceneSelector, conditionGameEvent, this.demoFile);

      if (conditionScenes.length > 0) {
        if (delayTick && this.demoFile.currentTick < delayTick) {
          if (condition.delayFilter) {
            this.log('scene rejected (delay)');
          } else {
            this.demoFile.gameEvents.once(targetGameEvent, checkCondition);
            this.log('re-registering listener');
          }
        } else {
          // todo add param to decide what scene to take / how to merge
          this.log('scene accepted');
          conditionScenes.forEach((conditionScene: Scene) => {
            // eslint-disable-next-line no-param-reassign
            conditionScene.startTick = scene.startTick;
            this.addScene(conditionScene);
          });
        }
      } else {
        this.log('scene rejected');
      }
    };

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore see todo above
    this.demoFile.gameEvents.once(targetGameEvent, checkCondition);
  }

  private checkTimeoutCondition(condition: TimeoutCondition, sourceGameEvent: DemoEvent, scene: Scene) {
    const conditionSceneSelector = condition.sceneSelector(sourceGameEvent, scene); // initalize the condition with the source game event to pass important params like a steam64Id
    const conditionEventName: string = conditionSceneSelector.eventName;

    // tick when the condition may resolve
    let delayTick: number | null = null;
    if (condition.delay) {
      delayTick = scene.eventTick + Math.ceil(condition.delay * this.demoFile.tickRate);
    }

    // tick when the condition fails if unresolved
    let timeoutTick: number = scene.endTick;
    if (condition.timeout) {
      timeoutTick = scene.eventTick + Math.ceil(condition.timeout * this.demoFile.tickRate);
    }

    const checkCondition = (conditionGameEvent: DemoEvent) => {
      const conditionScene = conditionSceneSelector.select(conditionSceneSelector, conditionGameEvent, this.demoFile);

      if (conditionScene && this.demoFile.currentTick < timeoutTick) {
        if (delayTick && this.demoFile.currentTick < delayTick) {
          if (condition.delayFilter) {
            this.log('scene rejected (delay)');
          } else {
            this.demoFile.gameEvents.once(conditionEventName, checkCondition);
            this.log('re-registering listener');
          }
        } else {
          this.log('scene accepted');
          this.addScene(scene);
        }
      } else if (this.demoFile.currentTick > timeoutTick) {
        this.log('scene rejected (timeout)');
      } else {
        this.demoFile.gameEvents.once(conditionEventName, checkCondition);
        this.log('re-registering listener');
      }
    };

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore todo see comment above
    this.demoFile.gameEvents.once(conditionEventName, checkCondition);
  }

  private getSceneSelectors(sceneSelectorConfigs: SceneSelectorConfig[]): SceneSelector[] {
    if (sceneSelectorConfigs.length === 0) {
      throw new Error('Empty Scene Selector Config.');
    }

    return sceneSelectorConfigs.map((sceneSelectorParams: SceneSelectorConfig) => {
      /* eslint-disable @typescript-eslint/ban-ts-comment */
      switch (sceneSelectorParams.name) {
        case 'KillByWeaponAndPlayerSelector':
          // @ts-ignore
          return new KillByWeaponAndPlayerSelector(sceneSelectorParams as KillByWeaponAndPlayerSelectorParams);
        case 'KillByWeaponSelector':
          // @ts-ignore
          return new KillByWeaponSelector(sceneSelectorParams as KillByWeaponSelectorParams);
        case 'KillByPlayerSelector':
          // @ts-ignore
          return new KillByPlayerSelector(sceneSelectorParams as KillByPlayerSelectorParams);
        case 'KillSelector':
          return new KillSelector(sceneSelectorParams);
        case 'ZoomInSelector':
          return new ZoomInSelector(sceneSelectorParams);
        case 'ZoomInAndKillSelector':
          // @ts-ignore
          return new ZoomInAndKillSelector(sceneSelectorParams as ZoomInAndKillSelectorParams);
        case 'ZoomInByPlayerSelector':
          // @ts-ignore
          return new ZoomInByPlayerSelector(sceneSelectorParams as ZoomInByPlayerSelectorParams);
        case 'ZoomInAndDelayAndKillSelector':
          // @ts-ignore
          return new ZoomInAndDelayAndKillSelector(sceneSelectorParams as ZoomInAndDelayAndKillSelectorParams);
        case 'XKillsPerRoundByPlayerSelector':
          // @ts-ignore
          return new XKillsPerRoundByPlayerSelector(sceneSelectorParams as XKillsPerRoundByPlayerSelectorSelectorParams);
        case 'XKillsPerRoundSelector':
          // @ts-ignore
          return new XKillsPerRoundSelector(sceneSelectorParams as XKillsPerRoundSelectorParams);
          /* eslint-enable @typescript-eslint/ban-ts-comment */
        default:
          throw new Error(`Unknown SceneSelector ${sceneSelectorParams.name}`);
      }
    });
  }

  private initLogServerConnection(logPort: number | undefined) {
    if (logPort) {
      const logServerConnection = new WebSocket(`ws://localhost:${logPort}`);

      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      logServerConnection.onopen = (_e: Event) => {
        logServerConnection.send('Hello from SceneExtractor');
      };

      logServerConnection.on('close', () => {
        delete this.logServerConnection;
      });

      this.logServerConnection = logServerConnection;
    }
  }

  private log(message: string) {
    if (this.logServerConnection) {
      this.logServerConnection.send(message);
    } else {
      console.info(message);
    }
  }
}

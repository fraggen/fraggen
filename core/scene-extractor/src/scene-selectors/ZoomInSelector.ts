import { DemoFile, IEventWeaponZoom } from 'demofile';
import { DemoEvent } from '../types/DemoEvent';
import { SceneSelector, SceneSelectorParams } from './SceneSelector';

export class ZoomInSelector extends SceneSelector {
  name = 'Zoom In Selector';

  description = 'Selects any zoom in';

  eventName = DemoEvent.WEAPON_ZOOM;

  params: SceneSelectorParams;

  constructor(params: SceneSelectorParams) {
    super(params);
    this.params = params;
  }

  public select(sceneSelector: SceneSelector, event: IEventWeaponZoom, demoFile: DemoFile) {
    return [this.getScene(event.player.steam64Id, sceneSelector, event, demoFile)];
  }
}

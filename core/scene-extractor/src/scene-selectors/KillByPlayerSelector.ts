import { DemoFile, IEventPlayerDeath } from 'demofile';
import { DemoEvent } from '../types/DemoEvent';
import { SceneSelector, SceneSelectorParams } from './SceneSelector';

export interface KillByPlayerSelectorParams extends SceneSelectorParams {
  steam64Id: string
}

export class KillByPlayerSelector extends SceneSelector {
  description = 'Selects any kill of a specific Player';

  eventName = DemoEvent.PLAYER_DEATH;

  constructor(params: KillByPlayerSelectorParams) {
    super(params);
    this.name = `Any Kill By Player ${params.steam64Id} Selector`;
    this.params = params;
  }

  public select(sceneSelector: SceneSelector, event: IEventPlayerDeath, demoFile: DemoFile) {
    if (!event.attackerEntity || event.attackerEntity.steam64Id !== this.params.steam64Id) {
      return [];
    }

    return [this.getScene(event.attackerEntity.steam64Id, sceneSelector, event, demoFile)];
  }
}

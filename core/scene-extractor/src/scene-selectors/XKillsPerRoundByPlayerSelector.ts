import { DemoFile, IEventRoundEnd, Player } from 'demofile';
import { DemoEvent } from '../types/DemoEvent';
import { SceneSelector, SceneSelectorParams } from './SceneSelector';

export interface XKillsPerRoundByPlayerSelectorSelectorParams extends SceneSelectorParams {
  killTarget: number, steam64Id: string
}

// Selects any kill
export class XKillsPerRoundByPlayerSelector extends SceneSelector {
  name = 'Any Player with X Kills Selector';

  description = 'Selects any player with exactly X kills at round end - useless on its own since round start is unknown';

  eventName = DemoEvent.ROUND_END;

  params: XKillsPerRoundByPlayerSelectorSelectorParams;

  constructor(params: XKillsPerRoundByPlayerSelectorSelectorParams) {
    if (params.killTarget < 1) {
      throw new Error('killTarget must be positive!');
    }

    super(params);
    this.params = params;
  }

  public select(sceneSelector: SceneSelector, event: IEventRoundEnd, demoFile: DemoFile) {
    const currentRound = demoFile.gameRules.roundsPlayed || 1;
    return demoFile.entities.players
      .filter((player: Player) => player.steam64Id === this.params.steam64Id && player.matchStats[currentRound - 1].kills === this.params.killTarget)
      .map((player: Player) => this.getScene(player.steam64Id, sceneSelector, event, demoFile));
  }
}

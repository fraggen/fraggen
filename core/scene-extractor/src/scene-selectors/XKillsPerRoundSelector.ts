import {
  DemoFile, IEventRoundEnd, IEventRoundStart, Player,
} from 'demofile';
import RoundEndCondition from '../conditions/RoundEndCondition';
import { DemoEvent } from '../types/DemoEvent';
import { Scene } from '../types/Scene';
import { SceneSelector, SceneSelectorParams } from './SceneSelector';
import { XKillsPerRoundByPlayerSelector } from './XKillsPerRoundByPlayerSelector';

export interface XKillsPerRoundSelectorParams extends SceneSelectorParams {
  killTarget: number
}

// Selects scenes of any player who did X kills in one round (full round)
export class XKillsPerRoundSelector extends SceneSelector {
  description = 'Selects scenes of any player who did X kills in one round (selects full round)';

  eventName = DemoEvent.ROUND_START;

  params: XKillsPerRoundSelectorParams;

  constructor(params: XKillsPerRoundSelectorParams) {
    if (params.killTarget < 1) {
      throw new Error('killTarget must be positive!');
    }

    super(params);

    this.name = `${params.killTarget} Kills Per Round Selector`;

    const roundEndConditon: RoundEndCondition = new RoundEndCondition(
      `Kill ${params.killTarget} Players Before Round End Condition`,
      (_event: IEventRoundEnd, scene: Scene) => new XKillsPerRoundByPlayerSelector({
        killTarget: params.killTarget,
        steam64Id: scene.playerToSpec,
        eventLength: 0,
        introSeconds: 0,
        outroSeconds: 0,
      }),
    );

    this.conditions = [roundEndConditon];

    this.params = params;
  }

  public select(sceneSelector: SceneSelector, event: IEventRoundStart, demoFile: DemoFile) {
    return demoFile.entities.players.map((player: Player) => this.getScene(player.steam64Id, sceneSelector, event, demoFile));
  }
}

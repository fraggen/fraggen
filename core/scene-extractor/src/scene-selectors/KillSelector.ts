import { DemoFile, IEventPlayerDeath } from 'demofile';
import { DemoEvent } from '../types/DemoEvent';
import { SceneSelector, SceneSelectorParams } from './SceneSelector';

// Selects any kill
export class KillSelector extends SceneSelector {
  name = 'Any Kill Selector';

  description = 'Selects any kill';

  eventName = DemoEvent.PLAYER_DEATH;

  constructor(params: SceneSelectorParams) {
    super(params);
    this.params = params;
  }

  public select(sceneSelector: SceneSelector, event: IEventPlayerDeath, demoFile: DemoFile) {
    if (!event.attackerEntity) {
      return [];
    }

    return [this.getScene(event.attackerEntity.steam64Id, sceneSelector, event, demoFile)];
  }
}

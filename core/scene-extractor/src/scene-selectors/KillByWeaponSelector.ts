import { DemoFile, IEventPlayerDeath } from 'demofile';
import { DemoEvent } from '../types/DemoEvent';
import { SceneSelector, SceneSelectorParams } from './SceneSelector';

export interface KillByWeaponSelectorParams extends SceneSelectorParams {
  weapon: string
}

// Selects any kill by weapon
export class KillByWeaponSelector extends SceneSelector {
  description = 'Selects any kill by weapon';

  eventName = DemoEvent.PLAYER_DEATH;

  params: KillByWeaponSelectorParams;

  constructor(params: KillByWeaponSelectorParams) {
    super(params);
    this.name = `Any Kill By Weapon ${params.weapon} Selector`;
    this.params = params;
  }

  public select(sceneSelector: SceneSelector, event: IEventPlayerDeath, demoFile: DemoFile) {
    if (event.weapon !== this.params.weapon || !event.attackerEntity) {
      return [];
    }

    return [this.getScene(event.attackerEntity.steam64Id, sceneSelector, event, demoFile)];
  }
}

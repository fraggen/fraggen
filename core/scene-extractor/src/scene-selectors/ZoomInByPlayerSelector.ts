import { DemoFile, IEventWeaponZoom } from 'demofile';
import { DemoEvent } from '../types/DemoEvent';
import { SceneSelector, SceneSelectorParams } from './SceneSelector';

export interface ZoomInByPlayerSelectorParams extends SceneSelectorParams {
  steam64Id: string
}

// Selects any zoom in by a specific player
export class ZoomInByPlayerSelector extends SceneSelector {
  description = 'Selects any zoom in by a specific player';

  eventName = DemoEvent.WEAPON_ZOOM;

  params: ZoomInByPlayerSelectorParams;

  constructor(params: ZoomInByPlayerSelectorParams) {
    super(params);
    this.name = `Zoom In By Player ${params.steam64Id} Selector`;
    this.params = params;
  }

  public select(sceneSelector: SceneSelector, event: IEventWeaponZoom, demoFile: DemoFile) {
    if (event.player.steam64Id !== this.params.steam64Id) {
      return [];
    }

    return [this.getScene(event.player.steam64Id, sceneSelector, event, demoFile)];
  }
}

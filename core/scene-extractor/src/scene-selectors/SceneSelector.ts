import { DemoFile } from 'demofile';
import Condition from '../conditions/Condition';
import { DemoEvent } from '../types/DemoEvent';
import { Scene } from '../types/Scene';

export interface SceneSelectorParams {
  introSeconds: number,
  outroSeconds: number,
  eventLength: number,
  [key: string]: unknown // todo create proper SceneSelectorConfig interfaces and remove this hack
}

export class SceneSelector {
  name = 'Base Scene Selector Implementation';

  description = 'Does not select anything useful';

  eventName: DemoEvent = DemoEvent.EVENT;

  conditions: Condition[] = [];

  params: SceneSelectorParams;

  constructor(params: SceneSelectorParams) {
    this.params = params;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public select(sceneSelector: SceneSelector, event: any, demoFile: DemoFile): Scene[] {
    if (demoFile.players.length > 0) {
      return [this.getScene(demoFile.players[0].steam64Id, sceneSelector, event, demoFile)];
    }

    return [];
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  protected getScene(playerToSpec: string, sceneSelector: SceneSelector, event: any, demoFile: DemoFile): Scene {
    const startTick = Math.max(demoFile.currentTick - Math.floor(demoFile.tickRate * (this.params.introSeconds + this.params.eventLength / 2)), 0);
    const eventTick = demoFile.currentTick;
    const endTick = demoFile.currentTick + Math.ceil(demoFile.tickRate * (this.params.outroSeconds + this.params.eventLength / 2));
    const length = this.params.introSeconds + this.params.eventLength + this.params.outroSeconds;

    return {
      startTick, eventTick, endTick, playerToSpec, length, sceneSelector, event,
    };
  }
}

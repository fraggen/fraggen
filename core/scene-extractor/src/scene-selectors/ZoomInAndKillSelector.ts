import { DemoFile, IEventWeaponZoom } from 'demofile';
import { TimeoutCondition } from '../conditions/TimeoutCondition';
import { DemoEvent } from '../types/DemoEvent';
import { Scene } from '../types/Scene';
import { KillByPlayerSelector } from './KillByPlayerSelector';
import { SceneSelector, SceneSelectorParams } from './SceneSelector';
import { ZoomInSelector } from './ZoomInSelector';

export interface ZoomInAndKillSelectorParams extends SceneSelectorParams {
  killTimeoutSeconds: number
}

// Selects scenes of any player who zoomed in and then scored a kill within the killTimeoutSeconds
export class ZoomInAndKillSelector extends SceneSelector {
  name = 'Zoom In And Kill Selector';

  description = 'Selects scene of any player who zoomed in and then scored a kill within the killTimeoutSeconds';

  eventName = DemoEvent.WEAPON_ZOOM;

  params: ZoomInAndKillSelectorParams;

  constructor(params: ZoomInAndKillSelectorParams) {
    super(params);

    if (params.killTimeoutSeconds >= params.outroSeconds) {
      throw new Error('killTimeout must be smaller than outroSeconds!');
    }

    const timeoutCondition = new TimeoutCondition(
      'Kill By Player POST_EVENT',
      params.killTimeoutSeconds,
      (gameEvent: IEventWeaponZoom) => new KillByPlayerSelector({
        name: 'KillByPlayerSelector',
        steam64Id: gameEvent.player.steam64Id,
        eventLength: 0,
        introSeconds: 0,
        outroSeconds: 0,
      }),
    );

    this.conditions.push(timeoutCondition);
    this.params = params;
  }

  select(sceneSelector: SceneSelector, event: IEventWeaponZoom, demoFile: DemoFile): Scene[] {
    return new ZoomInSelector(this.params).select(sceneSelector, event, demoFile);
  }
}

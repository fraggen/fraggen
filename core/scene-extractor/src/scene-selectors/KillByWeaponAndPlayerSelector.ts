import { DemoFile, IEventPlayerDeath } from 'demofile';
import { DemoEvent } from '../types/DemoEvent';
import { SceneSelector, SceneSelectorParams } from './SceneSelector';

export interface KillByWeaponAndPlayerSelectorParams extends SceneSelectorParams {
  weapon: string, steam64Id: string,
  [key: string]: unknown // todo create proper SceneSelectorConfig interfaces and remove this hack
}

// Selects any kill by weapon and player
export class KillByWeaponAndPlayerSelector extends SceneSelector {
  description = 'Selects any kill by weapon and player';

  eventName = DemoEvent.PLAYER_DEATH;

  conditions = [];

  params: KillByWeaponAndPlayerSelectorParams;

  constructor(params: KillByWeaponAndPlayerSelectorParams) {
    super(params);
    this.name = `Any Kill By Player  ${params.steam64Id} With Weapon ${params.weapon} Selector`;
    this.params = params;
  }

  public select(sceneSelector: SceneSelector, event: IEventPlayerDeath, demoFile: DemoFile) {
    if (event.weapon !== this.params.weapon || !event.attackerEntity || event.attackerEntity.steam64Id !== this.params.steam64Id) {
      return [];
    }
    return [this.getScene(event.attackerEntity.steam64Id, sceneSelector, event, demoFile)];
  }
}

import { DemoFile, IEventWeaponZoom } from 'demofile';
import { TimeoutCondition } from '../conditions/TimeoutCondition';
import { DemoEvent } from '../types/DemoEvent';
import { Scene } from '../types/Scene';
import { KillByPlayerSelector } from './KillByPlayerSelector';
import { SceneSelector, SceneSelectorParams } from './SceneSelector';
import { ZoomInSelector } from './ZoomInSelector';

export interface ZoomInAndDelayAndKillSelectorParams extends SceneSelectorParams {
  delay: number, killTimeoutSeconds: number
}

// Selects scenes of any player who zoomed in, waited for the delay period and then scored a kill within the killTimeoutSeconds
export class ZoomInAndDelayAndKillSelector extends SceneSelector {
  name = 'Zoom In And Kill Selector';

  description = 'Selects scenes of any player who zoomed in, waited for the delay period and then scored a kill within the killTimeoutSeconds';

  eventName = DemoEvent.WEAPON_ZOOM;

  params: ZoomInAndDelayAndKillSelectorParams;

  constructor(params: ZoomInAndDelayAndKillSelectorParams) {
    if (params.delay >= params.killTimeoutSeconds) {
      throw new Error('killTimeout must be smaller than the delay!');
    }

    if (params.killTimeoutSeconds >= params.outroSeconds) {
      throw new Error('killTimeout must be smaller than outroSeconds!');
    }

    super(params);

    const timeoutCondition: TimeoutCondition = new TimeoutCondition(
      'Kill By Player POST_EVENT',
      params.killTimeoutSeconds,
      (gameEvent: IEventWeaponZoom) => new KillByPlayerSelector({
        name: 'KillByPlayerSelector',
        steam64Id: gameEvent.player.steam64Id,
        eventLength: 0,
        introSeconds: 0,
        outroSeconds: 0,
      }),
      params.delay,
      true, // if the condition is resolved before the delay it will fail), // initialize the killByPlayerSelector with the zooming players steam id
    );

    this.params = params;
    this.conditions.push(timeoutCondition);
  }

  select(sceneSelector: SceneSelector, event: IEventWeaponZoom, demoFile: DemoFile): Scene[] {
    return new ZoomInSelector(this.params).select(sceneSelector, event, demoFile);
  }
}

# @fraggen/scene-extractor
The scene-extractor uses EventSelectors to extract Scenes from csgo demo files.

## Installation
```npm install @fraggen/scene-extractor```

## EventSelectors
EventSelectors listen for [Events](https://wiki.alliedmods.net/Counter-Strike:_Global_Offensive_Events) and then execute a SceneSelectorFunction to check if a possible Scene is found. You can write selectors for complex series of events.

A EventSelector can contain multiple additional Conditions that need to be checked when a possible Scene is found . This way you can combine multiple EventSelectors into chains to find Scenes with complex conditions. For example a ```zoomInAndDelayAndKillSelector``` combines the ```zoomInSelector``` via a delayed Condition with the ```killByPlayerSelector```. The SteamId64 of the zooming player is used to initialize the Condition.
## Usage


```ts
const STEAM_64_ID = "76561198141234567";
const DEAGLE = "deagle"
const INTRO_SECONDS = 5;
const OUTRO_SECONDS = 5;
const KILL_DELAY_SECONDS = .1;
const DEATH_EVENT_SECONDS = 0;
const AFTER_ZOOM_TIMEOUT = 1;

const eventSelectors = [
    zoomInAndKillSelector(DEATH_EVENT_SECONDS, KILL_DELAY_SECONDS, INTRO_SECONDS, OUTRO_SECONDS),
    zoomInAndDelayAndKillSelector(DEATH_EVENT_SECONDS, AFTER_ZOOM_TIMEOUT, KILL_DELAY_SECONDS, INTRO_SECONDS, OUTRO_SECONDS),
    killByPlayerSelector(STEAM_64_ID, DEATH_EVENT_SECONDS, INTRO_SECONDS, OUTRO_SECONDS),
    killByWeaponSelector(DEAGLE, DEATH_EVENT_SECONDS, INTRO_SECONDS, OUTRO_SECONDS),
    killByWeaponAndPlayerSelector(DEAGLE, STEAM_64_ID, DEATH_EVENT_SECONDS, INTRO_SECONDS, OUTRO_SECONDS),
    zoomInSelector(DEATH_EVENT_SECONDS, INTRO_SECONDS, OUTRO_SECONDS),
    killSelector(DEATH_EVENT_SECONDS, INTRO_SECONDS, OUTRO_SECONDS)
];

const sceneExtractor = new SceneExtractor();

// Extract scenes from a single file

sceneExtractor.extractScenes("P:\\ath\\to\\demofile.dem", eventSelectors).then(console.log);
/* output:
    [{
        eventSelector: zoomInSelector,
        event: 'weapon_zoom',
        startTick: 15068,
        eventTick: 16142,
        endTick: 17291,
        playerToSpec: '76561198141234567',
        length: 10,
    }]
 */


// Extract Scenes from a folder

const DEMO_FOLDER = "G:\\SteamLibrary\\steamapps\\common\\Counter-Strike Global Offensive\\csgo\\replays";

fs.readdir(DEMO_FOLDER, async (_err, files) => {
    const allScenes: DemoScenes[] = [];

    for (const fileName of files) {
        const scenes: Scene[] = [];
        const file = DEMO_FOLDER + "\\" + fileName;

        if (file.endsWith(".dem")) {
            console.info("parsing " + file)
            await sceneExtractor.extractScenes(file, eventSelectors).then((fileScenes: Scene[]) => {scenes.push(...fileScenes)});
        }

        allScenes.push({file, scenes});
    }

    console.info(`Scene Extractor found ${allScenes.reduce((ammount: any, fileScenes: DemoScenes) => ammount + fileScenes.scenes.length, 0)} scenes.`)

    /* allScenes:
    [{
        eventSelector: zoomInSelector,
        event: 'weapon_zoom',
        startTick: 15068,
        eventTick: 16142,
        endTick: 17291,
        playerToSpec: '76561198141234567',
        length: 10,
    }]
 */
});
```

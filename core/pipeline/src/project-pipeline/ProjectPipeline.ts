import { DemoFile } from 'demofile';
import * as fs from 'fs';
import * as path from 'path';
import { FraggenDemo } from '../types/FraggenDemo';
import { FraggenProject } from '../types/FraggenProject';
import { getDemosFromFolder } from '../utils';

export class ProjectPipeline {
  // Creates a project folder and fraggen-project.json
  public async createProject(name: string, projectFolder: string, fps: number, width: number, height: number, description?: string): Promise<FraggenProject> {
    const projectFolderNormalized = path.normalize(projectFolder);

    if (fs.existsSync(projectFolderNormalized)) {
      console.warn('Warning: Project folder exists already.');

      if (fs.readdirSync(projectFolderNormalized).length > 0) {
        throw new Error('Project folder is not empty!');
      }
    }

    if (!fs.existsSync(projectFolderNormalized)) fs.mkdirSync(projectFolderNormalized, { recursive: true });

    const demoFolder = ProjectPipeline.getDemosPath(projectFolderNormalized);
    if (!fs.existsSync(demoFolder)) fs.mkdirSync(demoFolder, { recursive: true });

    const demos: FraggenDemo[] = [];
    const initProject: FraggenProject = {
      name, projectFolder: projectFolderNormalized, description, demos, sceneSelectors: [], fps, width, height,
    };

    this.saveProject(initProject);
    this.initDemoFolder(initProject).then((project: FraggenProject) => this.saveProject(project));

    return this.openProject(projectFolder);
  }

  public saveProject(project: FraggenProject) {
    fs.writeFileSync(ProjectPipeline.getProjectJsonPath(project.projectFolder), JSON.stringify(project, null, 2), { encoding: 'utf-8' });
  }

  public async openProject(projectFolder: string): Promise<FraggenProject> {
    const projectJson = fs.readFileSync(ProjectPipeline.getProjectJsonPath(projectFolder), { encoding: 'utf-8' });
    const project: FraggenProject = JSON.parse(projectJson); // todo validate

    return this.initDemoFolder(project);
  }

  public async addDemos(project: FraggenProject, demoPaths: string[]) {
    // eslint-disable-next-line no-restricted-syntax
    for (const demoPath of demoPaths) {
      const targetPath = ProjectPipeline.getDemosPath(project.projectFolder) + path.sep + path.basename(demoPath).replace('.dem', '') + path.sep;
      fs.mkdirSync(targetPath, { recursive: true });
      fs.copyFileSync(demoPath, targetPath + path.basename(demoPath));

      const sceneFolder = `${targetPath + path.sep}scenes`;
      fs.mkdirSync(sceneFolder, { recursive: true });

      // eslint-disable-next-line no-await-in-loop
      project.demos.push(await this.getFraggenDemo(targetPath + path.basename(demoPath)));
    }

    return project;
  }

  private async initDemoFolder(project: FraggenProject) {
    const demosPath = ProjectPipeline.getDemosPath(project.projectFolder);
    const demoFiles = getDemosFromFolder(demosPath).map((file: string) => demosPath + path.sep + file);

    // eslint-disable-next-line no-restricted-syntax
    for (const demoFile of demoFiles) {
      if (!project.demos.some((demo: FraggenDemo) => demo.file === demoFile)) {
        // eslint-disable-next-line no-await-in-loop
        await this.getFraggenDemo(demoFile).then((fraggenDemo: FraggenDemo) => project.demos.push(fraggenDemo));
      }
    }

    return project;
  }

  private async getFraggenDemo(demoPath: string): Promise<FraggenDemo> {
    const header = await this.getDemoHeader(demoPath);

    return {
      name: path.basename(demoPath),
      map: header.mapName,
      file: demoPath,
      scenes: [],
      checked: true,
    };
  }

  private getDemoHeader(demoPath: string) {
    const demoFile: DemoFile = new DemoFile();

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return new Promise<any>((resolve) => {
      demoFile.on('start', () => {
        demoFile.cancel();
        resolve(demoFile.header);
      });

      demoFile.parseStream(fs.createReadStream(demoPath));
    });
  }

  private static getProjectJsonPath(projectFolder: string): string {
    return `${projectFolder}${path.sep}fraggen-project.json`;
  }

  private static getDemosPath(projectFolder: string): string {
    return `${projectFolder + path.sep}demos`;
  }
}

import { DemoScenes, Scene, SceneSelectorConfig } from '@fraggen/scene-extractor';
import { cpus, pool, WorkerPool } from 'workerpool';
import { RawData, WebSocket, WebSocketServer } from 'ws';
import { FraggenDemo } from '../types/FraggenDemo';
import { FraggenProject } from '../types/FraggenProject';
import { FraggenScene } from '../types/FraggenScene';
import { getDemosFromFolder, getSceneCount } from '../utils';

export class SceneExtractionPipeline {
  private workerPool: WorkerPool;

  private webSocketServer: WebSocketServer | undefined;

  private logPort: number | undefined;

  private logSubscriber: WebSocket | undefined;

  constructor(workerLimit?: number, logPort?: number) {
    this.logPort = logPort;
    this.initLoggingWebSocketServer();
    const maxWorkers = Math.min(workerLimit || cpus, cpus);
    this.workerPool = pool(`${__dirname}/SceneExtractorWorker.js`, { minWorkers: 1, maxWorkers, workerType: 'process' });
  }

  public async extractScenesFromProject(project: FraggenProject): Promise<FraggenProject> {
    if (project.demos.length === 0) {
      throw new Error('No demos in project!');
    }

    if (!project.sceneSelectors || project.sceneSelectors.length === 0) {
      throw new Error('No SceneSelector in project!');
    }
    const demos = project.demos.filter((demo: FraggenDemo) => demo.checked).map((demo: FraggenDemo) => demo.file);
    const demoScenesArray = await this.extractScenes(demos, project.sceneSelectors);

    return this.addDemoScenes(demoScenesArray, project);
  }

  public async extractScenesFromFolder(folder: string, sceneSelectorConfig: SceneSelectorConfig[]): Promise<DemoScenes[]> {
    const demos = getDemosFromFolder(folder).map((demo) => `${folder}\\${demo}`);

    if (demos.length === 0) {
      throw new Error('No demos found!');
    }

    return this.extractScenes(demos, sceneSelectorConfig);
  }

  public async extractScenes(demos: string[], sceneSelectorConfig: SceneSelectorConfig[]): Promise<DemoScenes[]> {
    this.log(`Parsing ${demos.length} demos.`);

    const scenePromises: Promise<DemoScenes>[] = demos.map(async (demo: string) => this.workerPool.exec('extractScenes', [demo, sceneSelectorConfig, this.logPort]));

    const result = await Promise.all(scenePromises);
    await this.workerPool.terminate();

    this.log(`Scene Extractor found ${getSceneCount(result)} scenes in ${result.length} demos.`);

    return result;
  }

  private addDemoScenes(demoScenesArray: DemoScenes[], project: FraggenProject) {
    demoScenesArray.forEach((demoScenes: DemoScenes) => {
      const demo = project.demos.find((_demo: FraggenDemo) => _demo.file === demoScenes.file);
      if (!demo) return;

      const fraggenScenes: FraggenScene[] = demoScenes.scenes.map((scene: Scene, index: number) => ({
        ...scene,
        id: demo.scenes.length + index + 1,
        sceneSelector: scene.sceneSelector.name,
        event: scene.sceneSelector.eventName,
        checked: true,
        recorded: false,
      }));
      demo.scenes = demo.scenes.concat(fraggenScenes);
    });

    return project;
  }

  private initLoggingWebSocketServer() {
    if (this.logPort) {
      const port = this.logPort;

      this.webSocketServer = new WebSocketServer({ port });

      this.webSocketServer.on('connection', (ws: WebSocket) => {
        ws.on('message', (data: RawData) => {
          const message = data.toString();

          if (message === 'LogContext connected!') {
            this.logSubscriber = ws;
          } else if (this.logSubscriber) {
            this.logSubscriber.send(message);
          }
        });

        ws.on('close', () => {
          delete this.logSubscriber;
        });
      });
    }
  }

  private log(message: string) {
    if (this.logSubscriber) {
      this.logSubscriber.send(message);
    } else {
      console.info(message);
    }
  }
}

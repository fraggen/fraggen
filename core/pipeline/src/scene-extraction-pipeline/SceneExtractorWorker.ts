import { DemoScenes, SceneExtractor, SceneSelectorConfig } from '@fraggen/scene-extractor';
import { worker } from 'workerpool';

function extractScenes(demo: string, sceneSelectorConfig: SceneSelectorConfig[], logPort?: number): Promise<DemoScenes> {
  const sceneExtractor = new SceneExtractor();
  return sceneExtractor.extractScenes(demo, sceneSelectorConfig, logPort);
}

// create a worker and register public functions
worker({
  extractScenes,
});

export interface FraggenScene {
  id: number
  sceneSelector: string
  event: string
  startTick: number
  eventTick: number
  endTick: number
  playerToSpec: string
  length: number
  recorded: boolean
  folder?: string
  checked: boolean
}

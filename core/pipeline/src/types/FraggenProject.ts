import { SceneSelectorConfig } from '@fraggen/scene-extractor';
import { FraggenDemo } from './FraggenDemo';

export interface FraggenProject {
  name: string
  description?: string
  projectFolder: string
  demos: FraggenDemo[]
  sceneSelectors: SceneSelectorConfig[]
  soundtrackFile?: string
  fps: number
  width: number
  height: number
}

import { FraggenScene } from './FraggenScene';

export interface FraggenDemo {
  name: string
  map: string
  file: string
  scenes: FraggenScene[]
  checked: boolean
}

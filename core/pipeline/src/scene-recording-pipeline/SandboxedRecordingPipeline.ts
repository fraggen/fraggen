import { DemoScenes } from '@fraggen/scene-extractor';
import { SandboxedRecorderParams, sleep } from '@fraggen/scene-recorder';
import { pool, WorkerPool } from 'workerpool';
import { getSceneCount } from '../utils';
import { RecordingPipeline } from './RecordingPipeline';

const workerStartDebounce = 3000; // todo make param

/* eslint-disable no-underscore-dangle */
export class SandboxedRecordingPipeline extends RecordingPipeline {
  public readonly recorderParams: SandboxedRecorderParams;

  private workerPool: WorkerPool;

  constructor(recordingOptions: SandboxedRecorderParams) {
    super(recordingOptions);

    this.recorderParams = recordingOptions;
    this.workerPool = pool(`${__dirname}/SceneRecorderWorker.js`, { minWorkers: 1, maxWorkers: this.recorderParams.sandboxieOptions.workerCount, workerType: 'process' });

    // eslint-disable-next-line no-promise-executor-return
    this.ready = new Promise((resolve) => resolve);
  }

  protected init() {
    for (let i = 1; i <= this.recorderParams.sandboxieOptions.workerCount; i++) {
      this.sandboxes.push(i);
    }
    this.ready = this.sceneRecorder.ready;
  }

  public async recordScenes(demoScenesArrayInput: DemoScenes[]): Promise<DemoScenes[]> {
    console.info(`Recording ${getSceneCount(demoScenesArrayInput)} scenes from ${demoScenesArrayInput.length} demos. Using ${this.recorderParams.sandboxieOptions.workerCount} workers`);

    const demoScenesArray: DemoScenes[] = demoScenesArrayInput.slice(0); // fastest way to copy an array / don't mutate input
    const result: DemoScenes[] = [];

    do {
      while (this.sandboxes.length === 0) { // wait for a sandbox to be available
        // eslint-disable-next-line no-await-in-loop
        await sleep(5000);
        console.debug(`waiting for free worker to record ${demoScenesArray[0].scenes.length} scenes from ${demoScenesArray[0].file}.`);
        console.debug(`${demoScenesArray.length} demos left.`);
      }
      console.debug('worker ready!');

      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      const __sandboxId__ = this.sandboxes.shift()!; // get a sandbox id from the pool
      const demoScenes = demoScenesArray.shift(); // get the next demo
      const sandboxedRecordingOptions: SandboxedRecorderParams = {
        ...this.recorderParams,
        telnetPort: this.recorderParams.telnetPort + __sandboxId__,
        sandboxieOptions: { ...this.recorderParams.sandboxieOptions, __sandboxId__ },
      };

      if (this.recorderParams.recorderLogPorts) {
        sandboxedRecordingOptions.recorderLogPort = this.recorderParams.recorderLogPorts[__sandboxId__];
      }

      this.workerPool.exec('recordScenes', [demoScenes, sandboxedRecordingOptions]).then((demoScenesResult: DemoScenes) => {
        this.sandboxes.push(__sandboxId__); // return sandbox to the pool
        result.push(demoScenesResult);
      });

      // eslint-disable-next-line no-await-in-loop
      await sleep(workerStartDebounce);
    } while (demoScenesArray.length > 0);

    await this.workerPool.terminate();
    return result;
  }
}

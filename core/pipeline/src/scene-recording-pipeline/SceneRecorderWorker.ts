import { DemoScenes } from '@fraggen/scene-extractor';
import { SandboxedRecorderParams, SandboxedSceneRecorder } from '@fraggen/scene-recorder';
import { worker } from 'workerpool';

async function recordScenes(demoScenes: DemoScenes, sandboxedRecordingOptions: SandboxedRecorderParams): Promise<DemoScenes> {
  const sceneExtractor = new SandboxedSceneRecorder(sandboxedRecordingOptions);
  await sceneExtractor.ready;
  return sceneExtractor.recordScenes(demoScenes);
}

// create a worker and register public functions
worker({
  recordScenes,
});

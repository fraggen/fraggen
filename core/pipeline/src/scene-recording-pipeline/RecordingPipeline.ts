import { DemoScenes, Scene, SceneSelector } from '@fraggen/scene-extractor';
import { SceneRecorder, RecorderParams } from '@fraggen/scene-recorder';
import * as path from 'path';
import { FraggenDemo } from '../types/FraggenDemo';
import { FraggenProject } from '../types/FraggenProject';
import { FraggenScene } from '../types/FraggenScene';
import { getSceneCount } from '../utils';

export class RecordingPipeline {
  public ready: Promise<void>;

  public recorderParams: RecorderParams;

  protected sceneRecorder: SceneRecorder;

  // only used by SandboxedRecordingPipeline
  protected sandboxes: number[] = [];

  constructor(recordingOptions: RecorderParams) {
    this.recorderParams = recordingOptions;

    this.ready = new Promise(() => { /* placeholder to allow overriding init method */ });
    this.sceneRecorder = {} as SceneRecorder;

    this.init();
  }

  protected init() {
    this.sceneRecorder = new SceneRecorder(this.recorderParams);
    this.ready = this.sceneRecorder.ready;
  }

  public async recordProject(project: FraggenProject): Promise<FraggenProject> {
    let demoScenesArray: DemoScenes[] = [];

    // todo: remove stupid type conversions, either go for one shared type or one type per workspace
    project.demos.forEach((demo: FraggenDemo) => {
      if (demo.checked) {
        const demoScene: DemoScenes = {
          file: demo.file,
          scenes: [],
          tickRate: 64, // todo demo.tickrate
        };

        demo.scenes.forEach((fraggenScene: FraggenScene) => {
          if (fraggenScene.checked) {
            const scene: Scene = {
              id: fraggenScene.id,
              folder: `${path.dirname(demo.file) + path.sep}scenes`,
              endTick: fraggenScene.endTick,
              event: fraggenScene.event,
              eventTick: fraggenScene.eventTick,
              length: fraggenScene.length,
              playerToSpec: fraggenScene.playerToSpec,
              sceneSelector: new SceneSelector({ introSeconds: 0, outroSeconds: 0, eventLength: 0 }),
              startTick: fraggenScene.startTick,
            };
            demoScene.scenes.push(scene);
          }
        });
        if (demoScene.scenes.length > 0) {
          demoScenesArray.push(demoScene);
        }
      }
    });

    if (demoScenesArray.length === 0) {
      console.warn('Nothing to record!');
      return project;
    }

    demoScenesArray = await this.recordScenes(demoScenesArray);

    demoScenesArray.forEach((demoScenes: DemoScenes) => {
      const fraggenDemo = project.demos.find((demo: FraggenDemo) => demo.file === demoScenes.file);
      demoScenes.scenes.forEach((scene: Scene) => {
        const fraggenScene = fraggenDemo?.scenes.find((fScene: FraggenScene) => fScene.id === scene.id);
        // eslint-disable-next-line  @typescript-eslint/no-non-null-assertion
        fraggenScene!.folder = scene.folder;
        // eslint-disable-next-line  @typescript-eslint/no-non-null-assertion
        fraggenScene!.recorded = true;
      });
    });

    return project;
  }

  public async recordScenes(demoScenesArray: DemoScenes[]): Promise<DemoScenes[]> {
    await this.ready;

    console.info(`Recording ${getSceneCount(demoScenesArray)} scenes from ${demoScenesArray.length} demos.`);

    const result: DemoScenes[] = [];
    // eslint-disable-next-line no-restricted-syntax
    for (const demoScenes of demoScenesArray) {
      // eslint-disable-next-line no-await-in-loop
      result.push(await this.sceneRecorder.recordScenes(demoScenes));
    }

    console.info(`Scene Extractor found ${getSceneCount(result)} scenes in ${result.length} demos.`);

    return result;
  }

  public closeCsgo() {
    this.sceneRecorder.closeCsgo();
  }
}

import { DemoScenes } from '@fraggen/scene-extractor';
import * as fs from 'fs';

export function getSceneCount(demoScenesArray: DemoScenes[]) {
  return demoScenesArray.reduce((length: number, demoScenes: DemoScenes) => length + demoScenes.scenes.length, 0);
}

export function getDemosFromFolder(folder: string): string[] {
  const files: string[] = fs.readdirSync(folder);

  return files.filter((file: string) => file.endsWith('.dem'));
}

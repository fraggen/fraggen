# @fraggen/scene-recorder
Lossless recording of extracted Scenes. Outputs one folder with TGAs and a WAV per scene.

## Installation
### Prerequisites
[HLAE](https://github.com/advancedfx/advancedfx/wiki/FAQ#how-to-install--uninstall-hlae)

### NPM
```npm install @fraggen/scene-recorder```

## Usage

```ts
const recordingOptions: SceneRecorderParams = {
  hlaePath, csgoPath, width: 1280, height: 720, fps: 60,
};
const sceneRecorder = new SceneRecorder(recordingOptions);

sceneRecorder.ready.then(async () => {
  sceneRecorder.recordScenes([{
    file: 'C:\\demos\\ace.dem',
    scenes: [{
      eventSelector: zoomInSelector,
      event: 'weapon_zoom',
      startTick: 15068,
      eventTick: 16142,
      endTick: 17291,
      playerToSpec: '76561198141234567',
      length: 10,
    }]).then((demoScenes: DemoScenes) => console.log);
});
```

## Recording with multiple CSGO instances
To speed up recording you can setup Sandboxie to enable the recorder to start csgo multiple times.
You obviously need good hardware to gain any speed advantage.

It is recommended to use the SandboxedRecordingPipeline instead of using the SandboxedSceneRecorder directly.
The pipeline handles creating and managing workers. The recorder is what those workers run. 

1. Download Sandboxie
2. To create as many boxes as you need edit `Sandboxie.ini:` and allow the disk with csgo and output disk:
   ```
      ...
      [1]
      Enabled=y
      BorderColor=#00ffff,ttl,6
      Template=SkipHook
      Template=qWave
      Template=LingerPrograms
      Template=AutoRecoverIgnore
      ConfigLevel=9
      BoxNameTitle=n
      AllowRawDiskRead=y
      OpenFilePath=C:\
      OpenFilePath=D:\
      
      [2]
      ...
   ```
3. Login to steam and enable autologin
4. Use the recorder with sandboxieOptions
5. Step 7
6. Profit

```ts
import SandboxedSceneRecorder from '@fraggen/scene-recorder/src/SandboxedSceneRecorder';
import SandboxedRecordingOptions from '@fraggen/scene-recorder/src/types/SandboxedRecordingOptions';

const sandboxieOptions: SandboxieOptions = {
  workerCount: 2,
  sandboxiePath,
  steamPath,
};

const sandboxedRecordingOptions: SandboxedRecordingOptions = {
  hlaePath,
  csgoPath,
  width: 1280,
  height: 720,
  fps: 60,
  sandboxieOptions,
  recordingPath,
};
const sceneRecorder = new SandboxedSceneRecorder(sandboxedRecordingOptions);

sceneRecorder.ready.then(async () => {
  sceneRecorder.recordScenes([{
    file: 'C:\\demos\\ace.dem',
    scenes: [{
      eventSelector: zoomInSelector,
      event: 'weapon_zoom',
      startTick: 15068,
      eventTick: 16142,
      endTick: 17291,
      playerToSpec: '76561198141234567',
      length: 10,
    },
      {
        file: 'C:\\demos\\ace.dem',
        scenes: [{
          eventSelector: zoomInSelector,
          event: 'weapon_zoom',
          startTick: 15068,
          eventTick: 16142,
          endTick: 17291,
          playerToSpec: '76561198141234567',
          length: 10,
        }])
    .then((demoScenes: DemoScenes) => console.log);
})
  ;
```

import useFakeTimers = jest.useFakeTimers;
import spyOn = jest.spyOn;
import { sleep } from '../src';

describe('sleep', () => {
  it('waits for selected period', () => {
    useFakeTimers();
    spyOn(global, 'setTimeout');

    const timeout = 499;

    sleep(timeout);
    expect(setTimeout).toHaveBeenCalledWith(expect.any(Function), timeout);
  });
});

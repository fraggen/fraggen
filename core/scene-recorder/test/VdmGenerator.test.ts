import {
  DemoScenes, Scene, SceneSelector, SceneSelectorParams,
} from '@fraggen/scene-extractor/src';
import * as path from 'path';
import * as fs from 'fs';
import { VdmGenerator, VdmGeneratorParams } from '../src/VdmGenerator';

const demoFile = `${__dirname}${path.sep}..${path.sep}..${path.sep}..${path.sep}test-resources${path.sep}strike.dem`;

describe('VdmGenerator', () => {
  it('can be instanced', () => {
    const params: VdmGeneratorParams = {
      csgoPath: '',
      fps: 0,
      startDemoCommands: [],
    };

    expect(() => new VdmGenerator(params)).not.toThrow();
  });

  it('generates VDM file with recording commands for each scene', () => {
    const params: VdmGeneratorParams = {
      csgoPath: `${__dirname}${path.sep}..${path.sep}..${path.sep}..${path.sep}..${path.sep}test-resources`,
      fps: 0,
      startDemoCommands: [],
    };

    const sceneSelectorParams: SceneSelectorParams = {
      eventLength: 0.5,
      introSeconds: 3,
      outroSeconds: 4,
    };

    const scene1: Scene = {
      id: 12345,
      sceneSelector: new SceneSelector(sceneSelectorParams),
      event: 'test',
      startTick: 46521,
      eventTick: 47521,
      endTick: 48521,
      playerToSpec: 'steam64Id',
      length: 20,
    };

    const demoScenes: DemoScenes = {
      file: demoFile,
      tickRate: 64,
      scenes: [scene1],
    };

    const vdmGenerator = new VdmGenerator(params);

    const demoName = path.basename(demoScenes.file).replace('.dem', '');
    const scriptName = `fraggen-record-${demoName}-${scene1.startTick}`;
    const expectedScriptPath = `${params.csgoPath}${path.sep}csgo${path.sep}cfg${path.sep}${scriptName}.cfg`;
    const expectedVdm = demoFile.replace('.dem', '.vdm');
    const expectedScripts = { generatedScripts: [expectedScriptPath, expectedVdm] };

    // eslint-disable-next-line @typescript-eslint/no-empty-function
    const spy = jest.spyOn(fs, 'writeFileSync').mockImplementation(() => {});

    const generatedScripts = vdmGenerator.generateVdm(demoScenes);

    expect(spy.mock.calls).toEqual([
      [expectedScriptPath, expect.any(String), { flag: 'w' }],
      [expectedVdm, expect.any(String), { flag: 'w' }],
    ]);
    // todo improve test
    expect(generatedScripts.vdmArray.length).toEqual(1);
    expect(generatedScripts.generatedScripts.length).toEqual(2);
    console.log(generatedScripts);
    expect(generatedScripts).toEqual(expectedScripts);
  });
});

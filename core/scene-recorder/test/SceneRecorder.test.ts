import { SceneRecorder, RecorderParams } from '../src';

describe('SceneRecorder', () => {
  it('can be instanced', () => {
    const params: RecorderParams = {
      csgoPath: '',
      fps: 0,
      height: 0,
      hlaePath: '',
      steamPath: '',
      telnetPort: 0,
      width: 0,
      commands: [],
    };

    expect(() => new SceneRecorder(params)).not.toThrow();
  });
});

import { SteamLoginUser } from './SteamLoginUser';

export interface SteamLoginUsers {
  [steam64Id: string] : SteamLoginUser
}

import { Scene } from '@fraggen/scene-extractor/src';

export interface RecordedScene extends Scene {
  folder: string
  recordingFps: number
}

export interface SandboxieOptions {
    sandboxiePath: string
    steamPath: string
    workerCount: number
    sandboxPrefix?: string
    __sandboxId__?: number
}

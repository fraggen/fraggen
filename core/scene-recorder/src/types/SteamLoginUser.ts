export interface SteamLoginUser {
  AccountName: string
  PersonaName: string
  RememberPassword: number
  WantsOfflineMode: number
  SkipOfflineModeWarning: number
  AllowAutoLogin: number
  MostRecent: number
  Timestamp: number
}

import { SandboxieOptions } from './SandboxieOptions';

export interface RecorderParams {
  hlaePath: string
  csgoPath: string
  steamPath: string
  telnetPort: number
  width: number
  height: number
  fps: number
  pipelineLogPort?: number
  recorderLogPort?: number
  commands?: []
  sandboxieOptions?: SandboxieOptions
}

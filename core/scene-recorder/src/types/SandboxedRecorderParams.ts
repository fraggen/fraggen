import { RecorderParams } from './RecorderParams';
import { SandboxieOptions } from './SandboxieOptions';

export interface SandboxedRecorderParams extends RecorderParams {
  sandboxieOptions: SandboxieOptions
  recorderLogPorts?: number[]
}

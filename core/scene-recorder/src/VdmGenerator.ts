import { DemoScenes, Scene } from '@fraggen/scene-extractor/src'; // todo either take the longer pipeline with test build or find a way to make tests use src while build uses dist
import * as fs from 'fs';
import * as path from 'path';
import { RecordedScene } from './types/RecordedScene';

export interface VdmGeneratorParams {
  startDemoCommands: string[]
  csgoPath: string
  fps: number
}

// how much time is worth skipping.
const SKIP_AHEAD_THRESHOLD_SECONDS = 10;

export class VdmGenerator {
  params: VdmGeneratorParams;

  constructor(params: VdmGeneratorParams) {
    this.params = params;
  }

  public generateVdm(demoScenes: DemoScenes, vdmArray: string[] = []): { generatedScripts: string[], vdmArray: string[] } {
    const generatedScripts: string[] = [];

    let vdm = 'demoactions\n{\n';
    let actionId = 1;
    let tick = demoScenes.tickRate; // esoteric csgo lore: don't start too early to prevent some bugs with some commands

    vdm += VdmGenerator.playCommands(
      this.params.startDemoCommands.join('; '),
      actionId,
      tick,
    );
    actionId++;
    tick += demoScenes.tickRate * 2; // wait a couple of ticks

    // overlapping scenes are recorded in another pass with a second vdm
    const nextRecordingPassScenes: number[] = [];

    demoScenes.scenes.forEach((sceneInput: Scene, sceneIndex: number) => {
      const demoName = path.basename(demoScenes.file).replace('.dem', '');
      const folder = `${sceneInput.folder}${path.sep}${demoName}${path.sep}${sceneInput.id}`;
      const scene: RecordedScene = { recordingFps: this.params.fps, folder, ...sceneInput };

      // scene is after current tick
      if (sceneIndex > 0 && scene.startTick < tick) {
        nextRecordingPassScenes.push(sceneInput.id!);

        // previous scene was last in current pass so exit the demo
        if (sceneIndex === demoScenes.scenes.length - nextRecordingPassScenes.length) {
          vdm += VdmGenerator.playCommands(
            'disconnect;',
            actionId,
            demoScenes.scenes[sceneIndex - 1].endTick + demoScenes.tickRate,
          );
          actionId++;
        }
        return;
      }

      // skip if threshold is reached
      if (scene.startTick - tick > SKIP_AHEAD_THRESHOLD_SECONDS * demoScenes.tickRate) {
        vdm += VdmGenerator.skipAhead(tick, scene.startTick - demoScenes.tickRate, actionId);
        actionId++;
        tick += demoScenes.tickRate;
      }

      // start commands are extracted into separate script files to work around issues with string escaping while invoking HLAE through VDM PlayCommand actions
      const startRecordingScriptName = this.createStartRecordingScript(scene, demoName, generatedScripts);
      vdm += VdmGenerator.playCommands(
        `exec ${startRecordingScriptName}`,
        actionId,
        scene.startTick,
      );
      actionId++;

      vdm += VdmGenerator.playCommands(
        'mirv_streams record end; host_timescale 1; mirv_snd_timescale default; host_framerate 0;',
        actionId,
        scene.endTick,
      );
      actionId++;

      if (sceneIndex + 1 === demoScenes.scenes.length - nextRecordingPassScenes.length) {
        vdm += VdmGenerator.playCommands(
          'disconnect;',
          actionId,
          scene.endTick + demoScenes.tickRate,
        );
        actionId++;
      }

      tick = scene.endTick + demoScenes.tickRate;
    });

    vdm += '\n}';
    vdmArray.push(vdm);
    if (nextRecordingPassScenes.length > 0) {
      return this.generateVdm(
        { ...demoScenes, scenes: demoScenes.scenes.filter((scene: Scene) => nextRecordingPassScenes.includes(scene.id!)) },
        vdmArray,
      );
    }

    return { generatedScripts, vdmArray };
  }

  private createStartRecordingScript(scene: RecordedScene, demoName: string, generatedScripts: string[]) {
    const startRecordingCommand = `mirv_streams record name "${scene.folder + path.sep + scene.id}"; spec_player_by_accountid ${scene.playerToSpec}; mirv_snd_timescale 1; host_timescale 0; host_framerate ${this.params.fps}; mirv_streams record start;`;
    const startRecordingScriptName = `fraggen-record-${demoName}-${scene.startTick}`;
    const startRecordingScriptLocation = `${this.params.csgoPath}${path.sep}csgo${path.sep}cfg${path.sep}${startRecordingScriptName}.cfg`;
    fs.writeFileSync(startRecordingScriptLocation, startRecordingCommand, { flag: 'w' });
    generatedScripts.push(startRecordingScriptLocation);
    return startRecordingScriptName;
  }

  private static skipAhead(startTick: number, skipToTick: number, actionId: number): string {
    let result = `\t"${actionId}"\n\t{\n`;
    result += '\t\tfactory "SkipAhead"\n';
    result += `\t\tname "Action${actionId}"\n`;
    result += `\t\tstarttick "${startTick}"\n`;
    result += `\t\tskiptotick "${skipToTick}"\n`;
    result += '\t}\n';

    return result;
  }

  private static playCommands(commands: string, actionId: number, startTick: number): string {
    let result = `\t"${actionId}"\n\t{\n`;
    result += '\t\tfactory "PlayCommands"\n';
    result += `\t\tname "Action${actionId}"\n`;
    result += `\t\tstarttick "${startTick}"\n`;
    result += `\t\tcommands "${commands}"\n`;
    result += '\t}\n';

    return result;
  }
}

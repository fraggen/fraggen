import { parse, stringify } from '@node-steam/vdf';
import {
  exec, execFile, ChildProcess,
} from 'child_process';
import * as fs from 'fs';
import { SceneRecorder } from './SceneRecorder';
import { SandboxedRecorderParams } from './types/SandboxedRecorderParams';
import { SteamLoginUser } from './types/SteamLoginUser';
import { SteamLoginUsers } from './types/SteamLoginUsers';
import { SteamLoginUsersVdf } from './types/SteamLoginUsersVdf';
import { sleep } from './utils';

/* eslint-disable no-underscore-dangle */
export class SandboxedSceneRecorder extends SceneRecorder {
  protected params: SandboxedRecorderParams;

  constructor(options: SandboxedRecorderParams) {
    super(options);

    if (options.sandboxieOptions.__sandboxId__ === undefined) {
      throw new Error('__sandboxId__ missing');
    }

    this.params = options;
  }

  protected async launchCsgo(): Promise<void> {
    if (!this.params.sandboxieOptions || !this.params.sandboxieOptions.__sandboxId__) {
      throw new Error('No sandboxie options.');
    }

    this.prepareSteamOfflineStart();

    // todo check paths with spaces
    const startSteamSandbox = `${this.params.sandboxieOptions.sandboxiePath}\\Start.exe /box:${this.params.sandboxieOptions.__sandboxId__} cmd /c "${this.params.sandboxieOptions.steamPath}\\steam.exe"`;

    await new Promise<void>((done) => {
      const process: ChildProcess = exec(startSteamSandbox, this.callback());

      process.on('exit', () => {
        this.log(`Sandbox ${this.params.sandboxieOptions.__sandboxId__}: launched steam with command: ${startSteamSandbox}`);
        done();
      });
    });

    await sleep(10000); // todo find some way to determine when steam startup finished in sandbox

    const hlaeParams = [
      '-csgoLauncher',
      '-noGui',
      '-autoStart',
      '-csgoExe',
      `${this.params.csgoPath}\\csgo.exe`,
      '-gfxEnabled',
      'true',
      '-gfxWidth',
      this.params.width.toString(),
      '-gfxHeight',
      this.params.height.toString(),
      '-gfxFull',
      'false',
      '-customLaunchOptions',
      this.csgoParams,
    ];

    const startCsgoSandbox = [`/box:${this.params.sandboxieOptions.__sandboxId__}`, 'cmd', '/c', `${this.params.hlaePath}\\HLAE.exe`, ...hlaeParams]; // todo move exe name into config
    return new Promise((done) => {
      const process: ChildProcess = execFile(`${this.params.sandboxieOptions.sandboxiePath}\\Start.exe`, startCsgoSandbox);

      process.on('exit', () => {
        this.log(`Sandbox ${this.params.sandboxieOptions.__sandboxId__}: launched HLAE with command: ${startCsgoSandbox}`);
        done();
      });
    });
  }

  protected log(message: string) {
    super.log(`Sandbox ${this.params.sandboxieOptions.__sandboxId__}: ${message}`);
  }

  // Sets WantsOfflineMode and SkipOfflineModeWarning to 1 in steams loginusers.vdf config.
  private prepareSteamOfflineStart() {
    const loginUsersFile = `${this.params.sandboxieOptions.steamPath}\\config\\loginusers.vdf`;
    const loginUsersString = fs.readFileSync(loginUsersFile);
    const loginUsers: SteamLoginUsersVdf = parse(loginUsersString.toString());

    let patchedUser = '';

    const patcher = (steamLoginUsers: SteamLoginUsers, [steam64Id, steamLoginUser]: [string, SteamLoginUser]) => {
      /* eslint-disable no-param-reassign */
      if (steamLoginUser.MostRecent) {
        steamLoginUser.WantsOfflineMode = 1;
        steamLoginUser.SkipOfflineModeWarning = 1;
        patchedUser = steamLoginUser.PersonaName;
      }

      steamLoginUsers[steam64Id] = steamLoginUser;
      /* eslint-enable no-param-reassign */

      return steamLoginUsers;
    };

    const patchedLoginUsers: SteamLoginUsersVdf = {
      users: Object.entries(loginUsers.users).reduce(patcher, {}),
    };

    const patchedLoginUsersString = stringify(patchedLoginUsers);
    fs.writeFileSync(loginUsersFile, patchedLoginUsersString);

    console.info(`Patched ${loginUsersFile}. Offline autostart activated for ${patchedUser}`);
  }
}

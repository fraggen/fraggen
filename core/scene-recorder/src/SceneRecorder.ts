import { DemoScenes } from '@fraggen/scene-extractor/src';
import {
  ChildProcess, exec, ExecException, execFile,
} from 'child_process';
import * as fs from 'fs';
import * as path from 'path';
import { Telnet } from 'telnet-client';
import { WebSocket, WebSocketServer } from 'ws';
import { RecorderParams } from './types/RecorderParams';
import { sleep } from './utils';
import { VdmGenerator } from './VdmGenerator';

export class SceneRecorder {
  public readonly ready: Promise<void>;

  protected readonly params: RecorderParams;

  protected readonly csgoParams: string;

  private readonly telnet: Telnet = new Telnet();

  private readonly vdmGenerator: VdmGenerator;

  private webSocketServer: WebSocketServer | undefined;

  private webSocketConnections: {ws: WebSocket, id: number}[] = [];

  private readonly startDemoCommands: string[] = [
    'sv_cheats 1',
    'engine_no_focus_sleep 0',
    'cl_draw_only_deathnotices 1',
    'cl_obs_interp_enable 0', // disable spectator transition between players
    'mirv_cmd clear',
    'mirv_streams remove base',
    'mirv_streams add baseFX base',
    'mirv_snd_filter block "+player\\\\vo\\\\\\*"', // I hate this, but it blocks some unwanted sounds
    'mirv_snd_filter block "+radio\\*"',
    'mirv_snd_filter block "ambient\\\\creatures\\\\chicken\\*"',
    // `fps_max ${this.options.fps}`
  ];

  constructor(params: RecorderParams) {
    this.params = params;
    this.csgoParams = `-insecure -console -netconport ${params.telnetPort}`;

    this.initLoggingWebSocketServer();

    if (params.commands) {
      this.startDemoCommands = this.startDemoCommands.concat(params.commands);
    }

    const telnetOptions = {
      host: '127.0.0.1',
      port: params.telnetPort,
      negotiationMandatory: false,
      timeout: 10,
      debug: true,
    };

    this.telnet.setMaxListeners(50);

    // eslint-disable-next-line no-async-promise-executor
    this.ready = new Promise(async (resolve) => {
      const init = async (startCsgo: boolean) => {
        try {
          await this.telnet.connect(telnetOptions);
          this.log('Successfully connected to CSGO!');

          this.execCommand('echo "game ready!"');
          await this.waitForOutput('game ready!');

          this.execCommand('snd_musicvolume 0'); // todo parameterize a list of csgo startup commands

          resolve();
        } catch (error) {
          this.log(`Connection failed. ${error}`);

          if (startCsgo) {
            this.log(`CSGO is not running on port ${this.params.telnetPort}, starting CSGO...`);
            await this.launchCsgo();
            await sleep(5000);
            init(false);
          } else {
            await sleep(3000);
            init(false);
          }
        }
      };

      this.log('Trying to connect to CSGO via telnet...');
      init(true);
    });

    this.vdmGenerator = new VdmGenerator({ startDemoCommands: this.startDemoCommands, ...params });
  }

  private initLoggingWebSocketServer() {
    if (this.params.recorderLogPort) {
      let port = this.params.recorderLogPort;
      // eslint-disable-next-line no-underscore-dangle
      if (this.params.sandboxieOptions && this.params.sandboxieOptions.__sandboxId__) {
        // eslint-disable-next-line no-underscore-dangle
        port += this.params.sandboxieOptions.__sandboxId__;
      }

      this.webSocketServer = new WebSocketServer({ port });
      this.webSocketServer.on('connection', (ws: WebSocket) => {
        const id = this.webSocketConnections.length;

        this.webSocketConnections.push({
          ws,
          id,
        });

        ws.on('close', () => {
          this.webSocketConnections = this.webSocketConnections.filter((connection) => connection.id !== id);
        });
      });
    }
  }

  public async recordScenes(demoScenes: DemoScenes): Promise<DemoScenes> {
    const { generatedScripts, vdmArray } = this.vdmGenerator.generateVdm(demoScenes);
    this.log(`Recording ${demoScenes.scenes.length} Scenes from ${demoScenes.file} in ${vdmArray.length} passes.`);

    // eslint-disable-next-line no-restricted-syntax
    for (const vdm of vdmArray) {
      const vdmLocation = `${path.dirname(demoScenes.file)}${path.sep}${path.basename(demoScenes.file).replace('.dem', '')}.vdm`;
      fs.writeFileSync(vdmLocation, vdm, { flag: 'w' });

      // eslint-disable-next-line no-await-in-loop
      await new Promise((resolve) => {
        this.execCommand(`playdemo ${demoScenes.file}`);

        this.waitForOutput('CSGO_GAME_UI_STATE_LOADINGSCREEN -> CSGO_GAME_UI_STATE_INGAME').then(() => {
          this.waitForOutput('CSGO_GAME_UI_STATE_INGAME -> CSGO_GAME_UI_STATE_MAINMENU').then(() => {

            resolve(undefined);
          });
        });
      });

      fs.unlinkSync(vdmLocation);
    }

    generatedScripts.forEach((script: string) => {
      // fs.unlinkSync(script);
      console.log(script);
    });

    return demoScenes;
  }

  public closeCsgo() {
    this.execCommand('quit');
  }

  protected async launchCsgo(): Promise<void> {
    const steamRunning = await new Promise<boolean>((done) => {
      exec('tasklist', (_error, stdout) => {
        done(stdout.includes('steam.exe'));
      });
    });

    // todo check paths with spaces
    if (!steamRunning) {
      this.log('Steam is not running, starting ...');
      const startSteam = `"${this.params.steamPath}\\steam.exe"`;

      await new Promise<void>((done) => {
        exec(startSteam, this.callback());
        this.log(`launched steam with command: ${startSteam}`);
        sleep(10000).then(done); // todo parameterize or find a way to determine when steam is ready
      });
    }

    const hlaeParams = [
      '-csgoLauncher',
      '-noGui',
      '-autoStart',
      '-csgoExe', `${this.params.csgoPath}\\csgo.exe`,
      '-gfxEnabled', 'true',
      '-gfxWidth', this.params.width.toString(),
      '-gfxHeight', this.params.height.toString(),
      '-gfxFull', 'false',
      '-customLaunchOptions', this.csgoParams,
    ];

    return new Promise((done) => {
      const process: ChildProcess = execFile(`${this.params.hlaePath}\\hlae.exe`, hlaeParams, this.callback());

      process.on('exit', () => {
        this.log(`launched HLAE with params: ${hlaeParams}`);
        done();
      });
    });
  }

  protected log(message: string) {
    if (this.webSocketConnections.length > 0) {
      this.webSocketConnections.forEach((connection) => connection.ws.send(message));
    } else {
      console.info(message);
    }
  }

  protected callback() {
    return (error: ExecException | null, stdout: string, stderr: string) => {
      if (error) {
        console.error(`error: ${error.message}`);
        return;
      }

      if (stderr) {
        console.error(`stderr: ${stderr}`);
        return;
      }

      console.log(`stdout:\n${stdout}`);
    };
  }

  private execCommand(command: string) {
    const telnetCommand = `${command};`;
    this.log(`Sending command: ${telnetCommand}`);
    this.telnet.exec(telnetCommand).catch(() => { /* catch timeout error */ });
  }

  private waitForOutput(targetString: string): Promise<null> {
    this.log(`Waiting for output: ${targetString}`);

    return new Promise((resolve) => {
      try {
        const listener = (data: string[]) => {
          if (data.toString().includes(targetString)) {
            this.telnet.removeListener('data', listener);
            this.log(`Output received: ${targetString}`);
            resolve(null);
          }
        };

        this.telnet.on('data', listener);
      } catch (e) {
        resolve(null);
      }
    });
  }
}

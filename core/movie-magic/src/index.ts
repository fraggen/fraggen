import pathToFfmpeg = require('ffmpeg-static');

import { RecordedScene } from '@fraggen/scene-recorder';
import { exec, ExecException } from 'child_process';

async function runFFMPEG(ffmpegParams: string[]) {
  await new Promise<void>((done) => {
    const process = exec(`"${pathToFfmpeg}" ${ffmpegParams.join(' ')}`, (error: ExecException | null, stdout: string, stderr: string) => {
      if (error) {
        console.error(`error: ${error.message}`);
        return;
      }

      if (stderr) {
        console.error(`stderr: ${stderr}`);
        return;
      }

      console.log(`stdout:\n${stdout}`);
    });

    process.on('exit', (error) => {
      console.log(error);
      console.log(`Finished ffmpeg with params: ${ffmpegParams.join(' ')}`);
      done();
    });
  });
}

async function encodeScene(scene: RecordedScene) {
  const ffmpegParams = [
    '-y',
    '-f image2',
    `-framerate ${scene.recordingFps}`,
    `-i "${scene.folder}\\base\\%05d.tga"`,
    `-i "${scene.folder}\\audio_044EB000.wav"`,
    '-vcodec libx264',
    '-qp 23',
    `-r ${scene.recordingFps}`,
    '-acodec libmp3lame',
    '-b:a 256K',
    `"${scene.folder}\\${scene.eventTick}.mp4"`,
  ];

  await runFFMPEG(ffmpegParams);
}

// eslint-disable-next-line @typescript-eslint/no-empty-function
encodeScene({ recordingFps: 60, folder: 'D:\\out\\match730_003272210663698596135_0278779657_189\\3\\take0000', eventTick: 1234 } as RecordedScene).then(() => {});

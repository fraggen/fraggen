# @fraggen/movie-magic
WIP

- detect different types of beats (high low mid), react to patterns e.g. low = zoom in, high = kill
- map beats to selected scenes via selector id (zoom in = high, kill = low)
- apply effects and cuts

You can use [Spleeter](https://github.com/deezer/spleeter) for audio source separation if the beat detecion

## Installation
```npm install @fraggen/movie-magic```

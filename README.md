# @fraggen
@fraggen is a collection of tools to generate csgo frag movies from demo files. 

The main packages are:
- [@fraggen/gui](https://gitlab.com/fraggen/fraggen/-/tree/master/ui/gui): electron app (wip)
- [@fraggen/cli](https://gitlab.com/fraggen/fraggen/-/tree/master/ui/cli): command line interface (wip)

They are composed of four core packages:
- [@fraggen/scene-extractor](https://gitlab.com/fraggen/fraggen/-/tree/master/core/scene-extractor): extracts Scenes via `EventSelectors` from demos 
- [@fraggen/scene-recorder](https://gitlab.com/fraggen/fraggen/-/tree/master/core/scene-recorder): seamless raw recording of extracted scenes
- [@fraggen/movie-magic](https://gitlab.com/fraggen/fraggen/-/tree/master/core/magic-magic): cutting, encoding, magic
- [@fraggen/pipeline](https://gitlab.com/fraggen/fraggen/-/tree/master/core/pipeline): combines the other core packages into convenient functions and provides multithreading / sandboxing



# Testing

If you want to test the work in progress version just clone the repository, change the paths and steam id in `ui/cli/src/index.ts` and run:

`npm install`

`npm run build`

`npm start -w ui/cli`

# Development

[Discord](https://discord.gg/PPVJV46D8P)

This is a multirepo utilizing typescript [project references](https://www.typescriptlang.org/docs/handbook/project-references.html) for building and npm [workspaces](https://docs.npmjs.com/cli/v7/using-npm/workspaces) for shared dependency management. You can use npm with `-w [workspace name]` to perform actions for a specific workspace. For example: `npm test -w core/pipeline`

There are six workspaces:
1. core/scene-extractor
2. core/scene-recorder
3. core/movie-magic
4. core/pipeline
5. ui/cli
6. ui/gui

NPM scripts probably wont work in linux but apart from paths there could be problems with the telnet server in the linux client and HLAE is not really tested on linux as well. Probably runs only in proton due to the hlae dependency.

## Building all packages

`npm run build` builds all projects in the correct order

## Building a specific package

`npm run build -w ui/gui`

## Testing (wip)

`npm test -w ui/gui`

# Credits
- [demofile](https://github.com/saul/demofile)
- [HLAE](https://github.com/advancedfx/advancedfx/wiki/FAQ#how-to-install--uninstall-hlae)

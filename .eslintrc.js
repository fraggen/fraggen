module.exports = {
  settings: {
    workspaces: {
      extensions: ['.ts', '.json'],
      sources: {
      },
    },
    'import/parsers': {
    //  '@typescript-eslint/parser': ['.ts', '.tsx'],
    },
    'import/resolver': {
      typescript: {
        alwaysTryTypes: true,
        project: [
          'core/scene-extractor/tsconfig.json',
          'core/scene-recorder/tsconfig.json',
          'core/movie-magic/tsconfig.json',
          'core/pipeline/tsconfig.json',
          'ui/cli/tsconfig.json',
          'ui/gui/tsconfig.json',
          'tsconfig.json',
        ],
      },
    },
  },
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    'airbnb-base',
    'plugin:@typescript-eslint/recommended',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: '2021',
    sourceType: 'module',
  },
  plugins: [
    '@typescript-eslint',
    'workspaces',
  ],
  rules: {
    'max-len': ['warn', { code: 300 }],
    'workspaces/no-relative-imports': 'error',
    'workspaces/require-dependency': 'warn',
    'import/no-extraneous-dependencies': [
      'warn',
    ],
    'import/extensions': 'off',
    'no-plusplus': 'off',
    'import/prefer-default-export': 'off',
    'no-console': 'off',
    'class-methods-use-this': 'off',
    'import/no-unresolved': [2, { ignore: ['@fraggen'] }], // todo create test eslintrc
  },
};

import { RecordingPipeline, SandboxedRecordingPipeline, SceneExtractionPipeline } from '@fraggen/pipeline';
import { DemoScenes, KillByWeaponAndPlayerSelectorParams, SceneSelectorConfig } from '@fraggen/scene-extractor';
import { XKillsPerRoundSelectorParams } from '@fraggen/scene-extractor/dist/scene-selectors/XKillsPerRoundSelector';
import { RecorderParams, SandboxedRecorderParams, SandboxieOptions } from '@fraggen/scene-recorder';

const demoFolder = 'D:\\csgo\\replays';
const csgoPath = 'D:\\csgo';
const sandboxiePath = 'D:\\Sandboxie-Plus';
const steamPath = 'C:\\Program Files (x86)\\Steam';
const hlaePath = 'D:\\HLAE';
// const recordingPath = 'D:\\out';
const steam64Id = '76561198141200296';
const weapon = 'deagle';
const introSeconds = 5;
const outroSeconds = 5;
const DEATH_EVENT_SECONDS = 0;
// const KILL_DELAY_SECONDS = 0.5; // seconds after zoom until kill may happen
// const KILL_TIMEOUT_SECONDS = 1; // seconds after zoom until kill must happen

const killByWeaponAndPlayer: KillByWeaponAndPlayerSelectorParams = {
  name: 'KillByWeaponAndPlayerSelector', weapon, steam64Id, eventLength: DEATH_EVENT_SECONDS, introSeconds, outroSeconds,
};

const xkillsPerRound: XKillsPerRoundSelectorParams = {
  introSeconds: 2,
  outroSeconds: 2,
  eventLength: 1,
  killTarget: 5,
};

const sceneSelectorConfig: SceneSelectorConfig[] = [
  { name: 'XKillsPerRoundSelector', ...xkillsPerRound },
  { name: 'KillByWeaponAndPlayerSelector', ...killByWeaponAndPlayer },
  // zoomInAndKillSelector(DEATH_EVENT_SECONDS, KILL_DELAY_SECONDS, INTRO_SECONDS, OUTRO_SECONDS),
  // zoomInAndDelayAndKillSelector(DEATH_EVENT_SECONDS, KILL_DELAY_SECONDS, KILL_TIMEOUT_SECONDS, INTRO_SECONDS, OUTRO_SECONDS),
  // killByPlayerSelector(steam64Id, DEATH_EVENT_SECONDS, INTRO_SECONDS, OUTRO_SECONDS),
  // killByWeaponSelector(DEAGLE, DEATH_EVENT_SECONDS, INTRO_SECONDS, OUTRO_SECONDS),
  // zoomInSelector(DEATH_EVENT_SECONDS, INTRO_SECONDS, OUTRO_SECONDS),
  // killSelector(DEATH_EVENT_SECONDS, INTRO_SECONDS, OUTRO_SECONDS),
];

const sceneExtractionPipeline = new SceneExtractionPipeline(20);
sceneExtractionPipeline.extractScenesFromFolder(demoFolder, sceneSelectorConfig).then(async (demoScenesArray: DemoScenes[]) => {
  const sandboxieOptions: SandboxieOptions = {
    workerCount: 3,
    sandboxiePath,
    steamPath,
  };

  const recordingOptions: RecorderParams = {
    hlaePath, csgoPath, telnetPort: 2121, width: 1280, height: 720, fps: 60, steamPath,
  };

  const sandboxedRecordingOptions: SandboxedRecorderParams = {
    ...recordingOptions, sandboxieOptions,
  };

  const sandboxedRecordingPipeline = new SandboxedRecordingPipeline(sandboxedRecordingOptions);
  console.log(await sandboxedRecordingPipeline.recordScenes(demoScenesArray));
  return;
  const recordingPipeline = new RecordingPipeline(recordingOptions);
  console.log(await recordingPipeline.recordScenes(demoScenesArray));
});

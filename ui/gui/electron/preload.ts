import { FraggenProject } from '@fraggen/pipeline';
import { SandboxedRecorderParams, RecorderParams } from '@fraggen/scene-recorder';
import { contextBridge, ipcRenderer } from 'electron';
import { PortFinderOptions } from 'portfinder';
import OpenDialogOptions = Electron.OpenDialogOptions;


contextBridge.exposeInMainWorld('electron', {
  selectDirectory: () => ipcRenderer.invoke('select-directory'),
  selectFiles: (options?: OpenDialogOptions) => ipcRenderer.invoke('select-files', options),
  readFile: (path: string) => ipcRenderer.invoke('read-file', path),
  createProject: (name: string, projectFolder: string, fps: number, width: number, height: number, description?: string) => ipcRenderer.invoke('create-project', name, projectFolder, fps, width, height, description),
  saveProject: (project: FraggenProject) => ipcRenderer.invoke('save-project', project),
  openProject: (projectFolder: string) => ipcRenderer.invoke('open-project', projectFolder),
  showFolder: (folder: string) => ipcRenderer.invoke('show-folder', folder),
  addDemos: (project: FraggenProject, demoPaths: string[]) => ipcRenderer.invoke('add-demos', project, demoPaths),
  getThreads: () => ipcRenderer.invoke('get-threads'),
  extractScenes: (sceneExtractorThreads: number, project: FraggenProject, logPort?: number) => ipcRenderer.invoke('extract-scenes', sceneExtractorThreads, project, logPort),
  recordProject: (sceneRecorderParams: RecorderParams, project: FraggenProject) => ipcRenderer.invoke('record-project', sceneRecorderParams, project),
  recordProjectSandboxed: (sandboxedRecordingOptions: SandboxedRecorderParams, project: FraggenProject) => ipcRenderer.invoke('record-project-sandboxed', sandboxedRecordingOptions, project),
  //startWebSocketServer: (port: number) => ipcRenderer.invoke('start-web-socket-server', port),
  getFreePort: (options?: PortFinderOptions) => ipcRenderer.invoke('get-free-port', options),
})

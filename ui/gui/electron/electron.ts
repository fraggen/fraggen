import { FraggenProject, ProjectPipeline, RecordingPipeline, SandboxedRecordingPipeline, SceneExtractionPipeline } from '@fraggen/pipeline';
import { SandboxedRecorderParams, RecorderParams } from '@fraggen/scene-recorder';
import electronIsDev from 'electron-is-dev';
import * as fs from "fs";
import portfinder, { PortFinderOptions } from 'portfinder';
import * as os from 'os';
import path from 'path';
import { BrowserWindow, app, protocol, ipcMain, dialog, globalShortcut, shell, IpcMainInvokeEvent } from 'electron';
import installExtension, { REACT_DEVELOPER_TOOLS } from 'electron-devtools-installer';
import OpenDialogOptions = Electron.OpenDialogOptions;
//import { WebSocketServer } from 'ws';

function createWindow() {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 1920,
    height: 1080,
    //fullscreen: true,
    webPreferences: {
      nodeIntegration: false,
      contextIsolation: true,
      sandbox: true,
      preload: path.join(__dirname, 'preload.js'),
    },
  });

  // and load the index.html of the app.
  mainWindow.loadURL(electronIsDev ? "http://localhost:3000" : "file://../index.html").then(() => electronIsDev ? mainWindow.webContents.openDevTools() : null);
}

const projectPipeline = new ProjectPipeline();
let recordingPipeline: RecordingPipeline;
let sandboxedRecordingPipeline: SandboxedRecordingPipeline;

function initRecordingPipeline(sceneRecorderParams: RecorderParams): Promise<void> {
  if (!recordingPipeline) {
    recordingPipeline = new RecordingPipeline(sceneRecorderParams);
  }

  return recordingPipeline.ready;
}

function initSandboxedRecordingPipeline(sandboxedRecordingOptions: SandboxedRecorderParams): void {
  if (!sandboxedRecordingPipeline) {
    sandboxedRecordingPipeline = new SandboxedRecordingPipeline(sandboxedRecordingOptions);
  }
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  const WEB_FOLDER = 'build';
  const PROTOCOL = 'file';

  protocol.interceptFileProtocol(PROTOCOL, (request, callback) => {
    let url = request.url.substr(PROTOCOL.length + 1);
    url = path.join(__dirname, WEB_FOLDER, url);
    url = path.normalize(url);
    console.log(url);
    callback({path: url});
  });

  installExtension(REACT_DEVELOPER_TOOLS)
    .then((name: string) => console.log(`Added Extension:  ${name}`))
    .catch((err: any) => console.log('An error occurred: ', err));

  createWindow();

  app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow();
  });

  if (!electronIsDev) {
    app.on('browser-window-focus', function () {
      globalShortcut.register("CommandOrControl+R", () => {
        console.log("CommandOrControl+R is pressed: Shortcut Disabled");
      });
      globalShortcut.register("F5", () => {
        console.log("F5 is pressed: Shortcut Disabled");
      });
    });
  }

  // IPC

  ipcMain.handle('select-directory', async (_event: IpcMainInvokeEvent) => {
    return dialog.showOpenDialog({ properties: ['openDirectory'] })
  });

  ipcMain.handle('select-files', async (_event: IpcMainInvokeEvent, options?: OpenDialogOptions) => {
    console.log(options);
    console.log(options);
    console.log(options);
    console.log(options);
    console.log(options);
    console.log(options);
    return dialog.showOpenDialog(options || { properties: ['openFile', 'multiSelections'], filters: [{ name: 'Demos', extensions: ['dem']}] })
  });

  ipcMain.handle('read-file', async (_event: IpcMainInvokeEvent, path: string) => {
    return fs.readFileSync(path);
  });

  ipcMain.handle('create-project', async (_event: IpcMainInvokeEvent, name: string, projectFolder: string, fps: number, width: number, height: number, description?: string) => {
    return projectPipeline.createProject(name, projectFolder, fps, width, height, description)
  });

  ipcMain.handle('save-project', async (_event: IpcMainInvokeEvent, project: FraggenProject) => {
    return projectPipeline.saveProject(project)
  });

  ipcMain.handle('open-project', async (_event: IpcMainInvokeEvent, projectFolder: string) => {
    return projectPipeline.openProject(projectFolder)
  })

  ipcMain.handle('show-folder', async (_event: IpcMainInvokeEvent, folder: string) => {
    console.log(folder);
    shell.showItemInFolder(folder);
  });

  ipcMain.handle('add-demos', async (_event: IpcMainInvokeEvent, project: FraggenProject, demoPaths: string[]) => {
    return projectPipeline.addDemos(project, demoPaths)
  });

  ipcMain.handle('get-threads', () => {
    return os.cpus().length
  });

  ipcMain.handle('extract-scenes', async (_event: IpcMainInvokeEvent, sceneExtractorThreads: number, project: FraggenProject, logPort?: number) => {
    const sceneExtractionPipeline = new SceneExtractionPipeline(sceneExtractorThreads, logPort);
    return await  sceneExtractionPipeline.extractScenesFromProject(project)
  });

  ipcMain.handle('record-project', async (_event: IpcMainInvokeEvent, sceneRecorderParams: RecorderParams, project: FraggenProject) => {
    await initRecordingPipeline(sceneRecorderParams);
    return await recordingPipeline.recordProject(project);
  });

  ipcMain.handle('record-project-sandboxed', async (_event: IpcMainInvokeEvent, sandboxedRecordingOptions: SandboxedRecorderParams, project: FraggenProject) => {
    console.log(sandboxedRecordingOptions);
    initSandboxedRecordingPipeline(sandboxedRecordingOptions);
    return await sandboxedRecordingPipeline.recordProject(project);
  });

  ipcMain.handle('get-free-port', async (_event: IpcMainInvokeEvent, options?: PortFinderOptions) => {
    return portfinder.getPortPromise(options);
  });

});

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

// In this file you can include the rest of your app"s specific main process
// code. You can also put them in separate files and require them here.

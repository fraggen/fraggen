
export enum MainLoggerState {
  IDLE = 'idle',
  WORKING = 'working',
}
interface MainLogger {
  state: MainLoggerState
  log: string[]
}

export default MainLogger;

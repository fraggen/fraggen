export enum WorkerLoggerState {
  STOPPED = 'stopped',
  STARTING = 'starting',
  READY = 'ready',
  RECORDING = 'recording',
}

interface WorkerLogger {
  state: WorkerLoggerState
  webSocketPort: number
  log: string[]
}

export default WorkerLogger;

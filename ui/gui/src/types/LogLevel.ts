enum LogLevel {
  LOG = 'log',
  WARNING = 'warning',
  ERROR =  'error',
  DEBUG = 'debug'
}

export default LogLevel;

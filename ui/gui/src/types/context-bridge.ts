import { FraggenProject } from '@fraggen/pipeline';
import { SandboxedRecorderParams, RecorderParams } from '@fraggen/scene-recorder';
import { OpenDialogReturnValue } from 'electron';
import { PortFinderOptions } from 'portfinder';
import OpenDialogOptions = Electron.OpenDialogOptions;

export {};

declare global {

  interface Window {
    electron: {
      selectDirectory: () => Promise<OpenDialogReturnValue>
      selectFiles: (options?: OpenDialogOptions) =>  Promise<OpenDialogReturnValue>
      readFile: (path: string) =>  Promise<Buffer>
      createProject: (name: string, projectFolder: string, fps: number, width: number, height: number, description?: string) => Promise<FraggenProject>
      saveProject: (project: FraggenProject) => Promise<void>
      openProject: (projectFolder: string) => Promise<FraggenProject>
      showFolder: (folder: string) => Promise<void>
      addDemos: (project: FraggenProject, demoPaths: string[]) => Promise<FraggenProject>
      getThreads: () => Promise<number>
      extractScenes: (sceneExtractorThreads: number, project: FraggenProject, logPort?: number) => Promise<FraggenProject>
      recordProject: (sceneRecorderParams: RecorderParams, project: FraggenProject) => Promise<FraggenProject>
      recordProjectSandboxed: (sandboxedRecordingOptions: SandboxedRecorderParams, project: FraggenProject) => Promise<FraggenProject>
      getFreePort: (options?: PortFinderOptions) => Promise<number>
    }
  }
}

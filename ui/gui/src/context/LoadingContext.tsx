import { createContext, useContext, useState } from 'react';
import { MainLoggerState } from '../types/MainLogger';
import { LogContext } from './LogContext';


interface LoadingContextType {
  isLoading: boolean
  setIsLoading: (value: boolean) => void,
}

export const LoadingContext = createContext({} as LoadingContextType);

export default function LoadingProvider({ children }: any) {
  const logContext = useContext(LogContext);

  const [isLoading, setIsLoadingInternal] = useState<boolean>(false);

  function setIsLoading(value: boolean) {
    if (value) {
      logContext.setMainLoggerState(MainLoggerState.WORKING);
    } else {
      logContext.setMainLoggerState(MainLoggerState.IDLE);
    }

    setIsLoadingInternal(value);
  }

  return (
    <LoadingContext.Provider
      value={{
        isLoading,
        setIsLoading
      }}
    >
      {children}
    </LoadingContext.Provider>
  )
}


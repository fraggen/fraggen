import { createContext, useState } from 'react';
import LogLevel from '../types/LogLevel';
import MainLogger, { MainLoggerState } from '../types/MainLogger';
import WorkerLogger, { WorkerLoggerState } from '../types/WorkerLogger';

interface LogContextType {
  mainLogger: MainLogger,
  recorderLogger: WorkerLogger | undefined,
  sandboxLoggers: WorkerLogger[],
  log: (value: string, level?: LogLevel) => void,
  connectRecorderLogger: (webSocketPort: number) => void,
  connectSandboxLogger: (webSocketPort: number) => void,
  setMainLoggerState: (value: MainLoggerState) => void
}

export const LogContext = createContext({} as LogContextType);


export default function LogProvider({ children }: any) {
  const [mainLogger, setMainLogger] = useState<MainLogger>({state: MainLoggerState.IDLE, log:[]});
  const [recorderLogger, setRecorderLogger] = useState<WorkerLogger | undefined>();
  const [sandboxLoggers, setSandboxLoggers] = useState<WorkerLogger[]>([]);

  function connectRecorderLogger(webSocketPort: number) {
    setRecorderLogger({
      state: WorkerLoggerState.STARTING,
      webSocketPort: webSocketPort,
      log: []
    })

    const onMessage = (event: MessageEvent) => {
      const data = event.data.toString();

      setRecorderLogger((prevState: WorkerLogger | undefined) => {
        const state = getRecorderState(data);

        if (prevState) {
            return {
              ...prevState,
              state,
              log: [...prevState.log, data]
            };
          }

          return
      });
    };

    connectWebSocket(webSocketPort, onMessage);
  }

  function connectSandboxLogger(webSocketPort: number) {
    const recorder = sandboxLoggers.find((recorder: WorkerLogger) => recorder.webSocketPort === webSocketPort)

    if (!recorder) {
      setSandboxLoggers((prevState: WorkerLogger[]) => {
        return [
          ...prevState,
          {
            state: WorkerLoggerState.STOPPED,
            webSocketPort,
            log: []
          }
        ];
      });
    } else if (recorder.state === WorkerLoggerState.STOPPED) {
      setSandboxLoggers((prevState: WorkerLogger[]) => {
        return [
          ...prevState.filter((recorder: WorkerLogger) => recorder.webSocketPort !== webSocketPort),
          {
            state: WorkerLoggerState.STOPPED,
            webSocketPort,
            log: []
          }
        ];
      });
    }

    if (!recorder || recorder.state === 'stopped') { // connect to web socket server and update log/state when the worker writes to the wss
      const onMessage = (event: MessageEvent) => {
        const data = event.data.toString();

        setSandboxLoggers((prevState: WorkerLogger[]) => {
          const prevRecorder = prevState.find((recorder: WorkerLogger) => recorder.webSocketPort === webSocketPort)

          if (prevRecorder) {
            let state = getRecorderState(data);

            return [
              ...prevState.filter((recorder: WorkerLogger) => recorder.webSocketPort !== webSocketPort),
              {
                state,
                webSocketPort: prevRecorder.webSocketPort,
                log: [...prevRecorder.log, data]
              }
            ];
          } else {
            return [
              ...prevState,
              {
                state: WorkerLoggerState.STARTING,
                webSocketPort,
                log: [data]
              }
            ];
          }
        });
      };

      connectWebSocket(webSocketPort, onMessage);
    }
  }

  function connectWebSocket(webSocketPort: number, onMessage: (event: MessageEvent) => void) {
    const webSocket = new WebSocket(getWebSocketServerUrl(webSocketPort));

    webSocket.onopen = function (_e: Event) {
      webSocket.send('LogContext connected!');
    };

    webSocket.onmessage = onMessage;

    webSocket.onclose = function (event: CloseEvent) {
      // todo set state?
      if (event.wasClean) {
        log(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
      } else {
        // e.g. server process killed or network down
        // event.code is usually 1006 in this case
        log('[close] Connection died', LogLevel.WARNING);
      }
    };

    webSocket.onerror = function (error: Event) {
      log(`[error] ${error.toString()}`, LogLevel.ERROR);
    };
  }

  function getWebSocketServerUrl(webSocketPort: number) {
    return 'ws://localhost:' + webSocketPort;
  }

  function log(value: string, level: LogLevel = LogLevel.LOG) {
    let message = value;

    switch (level) {
      case 'log':
        console.log(message);
        break
      case 'warning':
        message = 'Warning: ' + value;
        console.warn(message);
        break;
      case 'error':
        message = 'ERROR: ' + value;
        console.error(message);
        break;
      case 'debug': // todo add setting to disable debug logs
        message = 'DEBUG: ' + value;
        console.log('DEBUG: ' + value);
        break;
    }

    setMainLogger((currMainLogger) => ({state: currMainLogger.state, log: [...currMainLogger.log, message]}));
  }

  function setMainLoggerState(value: MainLoggerState) {
    setMainLogger((currMainLogger) => ({state: value, log: currMainLogger.log}));
  }

  function getRecorderState(data: string): WorkerLoggerState {
    let state: WorkerLoggerState = WorkerLoggerState.STOPPED;

    if (data.includes('Trying to connect to CSGO via telnet...')) {
      state = WorkerLoggerState.STARTING;
    } else if (data.includes('Output received: game ready!') || data.includes('Output received: CSGO_GAME_UI_STATE_INGAME -> CSGO_GAME_UI_STATE_MAINMENU')) {
      state = WorkerLoggerState.READY;
    } else if (data.includes('Recording')) {
      state = WorkerLoggerState.RECORDING;
    } else if (data.includes('Sending command: quit;')) {
      state = WorkerLoggerState.STOPPED;
      // todo cleanup?
    }

    return state;
  }

  return (
    <LogContext.Provider
      value={{
        sandboxLoggers,
        connectRecorderLogger,
        connectSandboxLogger,
        log,
        recorderLogger,
        mainLogger,
        setMainLoggerState
    }}
    >
      {children}
    </LogContext.Provider>
  );
}


import { createContext, useEffect, useState } from 'react';

interface SettingsContextType {
  sceneExtractorThreads: number
  setSceneExtractorThreads: (value: number) => void
  steamPath: string
  setSteamPath: (value: string) => void
  csgoPath: string
  setCsgoPath: (value: string) => void
  hlaePath: string
  setHlaePath: (value: string) => void
  sandboxiePath: string
  setSandboxiePath: (value: string) => void
  sandboxCount: number
  setSandboxCount: (value: number) => void
  showImages: boolean
  setShowImages: (value: boolean) => void
  viewWide: boolean
  setViewWide: (value: boolean) => void
}

export const SettingsContext = createContext({} as SettingsContextType);

export default function SettingsProvider({ children }: any) {
  const [sceneExtractorThreads, _setSceneExtractorThreads] = useState<number>(1);
  const [steamPath, _setSteamPath] = useState<string>('');
  const [csgoPath, _setCsgoPath] = useState<string>('');
  const [hlaePath, _setHlaePath] = useState<string>('');
  const [sandboxiePath, _setSandboxiePath] = useState<string>('');
  const [sandboxCount, _setSandboxCount] = useState<number>(2);
  const [showImages, _setshowImages] = useState(true);
  const [viewWide, _setViewWide] = useState(true);

  useEffect(() => {
    window.electron.getThreads()
      .then((value: number) => {
        const recommendedThreads = Math.max(value - 1, 1);
        initSetting('scene-extractor-threads', 'int', recommendedThreads, _setSceneExtractorThreads);
      });

    initSetting('steam-path', 'string', '', _setSteamPath); // todo create key constants
    initSetting('csgo-path', 'string', '', _setCsgoPath);
    initSetting('hlae-path', 'string', '', _setHlaePath);
    initSetting('sandboxie-path', 'string', '', _setSandboxiePath);
    initSetting('sandbox-count', 'int', 2, _setSandboxCount);
    initSetting('show-images', 'boolean', true, _setshowImages);
    initSetting('view-wide', 'boolean', true, _setViewWide);
  }, []);

  function initSetting(key: string, type: 'int' | 'string' | 'boolean', defaultValue: any, setter: Function) {
    let savedSetting: any = window.localStorage.getItem(key);
    if (savedSetting !== null) {
      if (type === 'int') {
        savedSetting = Number.parseInt(savedSetting, 10);
      } else if (type === 'boolean') {
        savedSetting = savedSetting === 'true';
      }
      setter(savedSetting);
    } else {
      setter(defaultValue);
    }
  }

  // todo generic setter
  function setSceneExtractorThreads(value: number) { // todo snackbar alert
    window.localStorage.setItem('scene-extractor-threads', value.toString());
    _setSceneExtractorThreads(value);
  }

  function setSteamPath(value: string) { // todo snackbar alert
    window.localStorage.setItem('steam-path', value);
    _setSteamPath(value);
  }

  function setCsgoPath(value: string) { // todo snackbar alert
    window.localStorage.setItem('csgo-path', value);
    _setCsgoPath(value);
  }

  function setHlaePath(value: string) { // todo snackbar alert
    window.localStorage.setItem('hlae-path', value);
    _setHlaePath(value);
  }

  function setSandboxiePath(value: string) { // todo snackbar alert
    window.localStorage.setItem('sandboxie-path', value);
    _setSandboxiePath(value);
  }

  function setSandboxCount(value: number) { // todo snackbar alert
    window.localStorage.setItem('sandbox-count', value.toString());
    _setSandboxCount(value);
  }

  function setShowImages(value: boolean) { // todo snackbar alert
    window.localStorage.setItem('show-images', value.toString());
    _setshowImages(value);
  }

  function setViewWide(value: boolean) { // todo snackbar alert
    window.localStorage.setItem('view-wide', value.toString());
    _setViewWide(value);
  }

  return (
    <SettingsContext.Provider
      value={{
        sceneExtractorThreads,
        setSceneExtractorThreads,
        steamPath,
        setSteamPath,
        csgoPath,
        setCsgoPath,
        hlaePath,
        setHlaePath,
        sandboxiePath,
        setSandboxiePath,
        sandboxCount,
        setSandboxCount,
        showImages,
        setShowImages,
        viewWide,
        setViewWide
      }}
    >
      {children}
    </SettingsContext.Provider>
  )
}


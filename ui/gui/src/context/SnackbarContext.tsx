import { createContext, useContext, useState } from 'react';
import LogLevel from '../types/LogLevel';
import { LogContext } from './LogContext';

interface SnackbarContextType {
  addSuccess: (message: string) => void
  successQueue: string[]
  addInfo: (message: string) => void
  infoQueue: string[]
  addWarning: (message: string) => void
  warningQueue: string[]
  addError: (message: string) => void
  errorQueue: string[]
}

export const SnackbarContext = createContext({} as SnackbarContextType);

export default function SnackbarProvider({ children }: any) {
  const logContext = useContext(LogContext);
  const [successQueue] = useState<string[]>([])
  const [infoQueue] = useState<string[]>([])
  const [warningQueue] = useState<string[]>([])
  const [errorQueue] = useState<string[]>([])

  function addSuccess(message: string) {
    logContext.log(message, LogLevel.LOG)
    successQueue.push(message)
  }

  function addInfo(message: string) {
    logContext.log(message, LogLevel.LOG)
    infoQueue.push(message)
  }

  function addWarning(message: string) {
    logContext.log(message, LogLevel.WARNING)
    warningQueue.push(message)
  }

  function addError(message: string) {
    logContext.log(message, LogLevel.ERROR)
    errorQueue.push(message)
  }

  return (
    <SnackbarContext.Provider
      value={{
        addSuccess,
        successQueue,
        addInfo,
        infoQueue,
        addWarning,
        warningQueue,
        addError,
        errorQueue,
      }}
    >
      {children}
    </SnackbarContext.Provider>
  )
}


import {FraggenDemo, FraggenProject, FraggenScene} from '@fraggen/pipeline';
import { OpenDialogReturnValue } from 'electron';
import { createContext, useContext, useState } from 'react';
import LogLevel from '../types/LogLevel';
import { LoadingContext } from './LoadingContext';
import { LogContext } from './LogContext';
import { SnackbarContext } from './SnackbarContext';

interface ProjectContextType {
  project: undefined | FraggenProject
  openProject: () => Promise<void>
  setProject: (project: FraggenProject) => void
  reloadProject: () => void
  saveProject: (project: FraggenProject) => Promise<void>
  getSceneCount: () => number
  getRecordedSceneCount: () => number
  getRecordedScenes: () => FraggenScene[]
  addDemos: () => Promise<void>
}

export const ProjectContext = createContext({} as ProjectContextType);

export default function ProjectProvider({ children }: any) {
  const logContext = useContext(LogContext);
  const loadingContext = useContext(LoadingContext);
  const snackbarContext = useContext(SnackbarContext);

  const [project, setProjectInternal] = useState<undefined | FraggenProject>()

  function openProject() {
    logContext.log('openProject', LogLevel.DEBUG);
    loadingContext.setIsLoading(true);

    return window.electron.selectDirectory().then((folder: OpenDialogReturnValue) => {
        const projectFolder = folder.filePaths[0];

        if (projectFolder === undefined) {
          return;
        }

        logContext.log('opening project: ' + projectFolder);

        return window.electron.openProject(projectFolder)
          .then((project: FraggenProject) => setProject(project))
          .catch((err: Error) => snackbarContext.addError(err.message));

      }).finally(() => loadingContext.setIsLoading(false));
  }

  function setProject(newProject: FraggenProject) {
    logContext.log('setProject', LogLevel.DEBUG);
    setProjectInternal({ ...newProject });
    document.title = '@fraggen - ' + newProject.name;
  }

  function reloadProject() {
    logContext.log('reloadProject', LogLevel.DEBUG);
    window.electron.openProject(project!.projectFolder).then((project: FraggenProject) => {
      setProject(project);
    })
  }

  function saveProject(freshProject: FraggenProject) {
    logContext.log('saveProject', LogLevel.DEBUG);
    return window.electron.saveProject(freshProject)
      .then(() => setProject(freshProject));
  }

  function addDemos(): Promise<void> {
    logContext.log('addDemos', LogLevel.DEBUG);
    loadingContext.setIsLoading(true);

    if (!project) {
      const message = 'no project!';
      logContext.log(message, LogLevel.ERROR);
      loadingContext.setIsLoading(false);
      throw new Error(message)
    }

    const demoCountBefore = project.demos.length;

    return window.electron.selectFiles()
      .then((files: OpenDialogReturnValue) => {
        return window.electron.addDemos(project, files.filePaths)
          .then((project: FraggenProject) => {
            const addedDemosCount = project.demos.length - demoCountBefore;

            if (addedDemosCount > 0) {
              saveProject(project).then(() => snackbarContext.addSuccess(`${addedDemosCount} demos added.`));
            } else {
              snackbarContext.addWarning('No new demos selected.');
            }
          })
          .catch((err: Error) => snackbarContext.addError(err.message));
      }).finally(() => loadingContext.setIsLoading(false));
  }

  function getSceneCount() {
    if (!project) {
      return 0;
    }

    return project.demos.reduce((count: number, demo: FraggenDemo) => count + demo.scenes.length, 0);
  }

  function getRecordedSceneCount() {
    if (!project) {
      return 0;
    }

    return project.demos.reduce((count: number, demo: FraggenDemo) => {
      return count + demo.scenes.reduce((count: number, scene: FraggenScene) => {
        return count + (scene.recorded ? 1 : 0);
      }, 0);
    }, 0);
  }

  function getRecordedScenes() {
    if (!project) {
      return [];
    }

    return project.demos.reduce((recordedScenes: FraggenScene[], demo: FraggenDemo) => {
      return recordedScenes.concat(demo.scenes.reduce((recordedScenes: FraggenScene[], scene: FraggenScene) => {
        if (scene.recorded) {
          return recordedScenes.concat([scene])
        }

        return recordedScenes;
      }, []));
    }, []);
  }

  return (
    <ProjectContext.Provider
      value={{
        project,
        openProject,
        setProject,
        reloadProject,
        saveProject,
        getSceneCount,
        getRecordedSceneCount,
        getRecordedScenes,
        addDemos
      }}
    >
      {children}
    </ProjectContext.Provider>
  )
}


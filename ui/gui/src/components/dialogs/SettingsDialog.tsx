
import { Save } from '@mui/icons-material';
import { Dialog, DialogActions, DialogContent, DialogTitle, IconButton, SxProps, TextField, } from '@mui/material';
import { ChangeEvent, useContext } from 'react';
import { SettingsContext } from '../../context/SettingsContext';


interface SettingsDialogProps {
  open: boolean
  close: () => void
}

const textFieldStyle: SxProps = {
  width: '400px'
};

function SettingsDialog({ open, close }: SettingsDialogProps) {
  const settingsContext = useContext(SettingsContext);

  return (
    <>
      <Dialog
        open={open}
        maxWidth="md"
        onClose={close}
        sx={{ minWidth: 275}}
      >
        <DialogTitle>Settings</DialogTitle>

        <DialogContent
          sx={{
            display: 'flex',
            flexDirection: 'column',
            m: 'auto',
            width: 'fit-content',
          }}
        >
          <TextField
            label="Scene Extractor Threads"
            id="scene-extractor-threads-input"
            margin="normal"
            variant="standard"
            type="number"
            value={settingsContext.sceneExtractorThreads}
            onChange={(change: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => settingsContext.setSceneExtractorThreads(Number.parseInt(change.target.value, 10))}
            sx={textFieldStyle}
          />
          <TextField
            label="Steam Path"
            id="steam-path-input"
            margin="normal"
            variant="standard"
            type="string"
            value={settingsContext.steamPath}
            onChange={(change: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => settingsContext.setSteamPath(change.target.value)}
            sx={textFieldStyle}
          />
          <TextField
            label="CSGO Path"
            id="csgo-path-input"
            margin="normal"
            variant="standard"
            type="string"
            value={settingsContext.csgoPath}
            onChange={(change: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => settingsContext.setCsgoPath(change.target.value)}
            sx={textFieldStyle}
          />
          <TextField
            label="HLAE Path"
            id="hlae-path-input"
            margin="normal"
            variant="standard"
            type="string"
            value={settingsContext.hlaePath}
            onChange={(change: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => settingsContext.setHlaePath(change.target.value)}
            sx={textFieldStyle}
          />
          <TextField
            label="Sandboxie Path"
            id="sandboxie-path-input"
            margin="normal"
            variant="standard"
            type="string"
            value={settingsContext.sandboxiePath}
            onChange={(change: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => settingsContext.setSandboxiePath(change.target.value)}
            sx={textFieldStyle}
          />
          <TextField
            label="Sandbox Count"
            id="sandbox-count-input"
            margin="normal"
            variant="standard"
            type="number"
            value={settingsContext.sandboxCount}
            onChange={(change: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => settingsContext.setSandboxCount(Number.parseInt(change.target.value, 10))}
            sx={textFieldStyle}
          />
        </DialogContent>

        <DialogActions>
          <IconButton>
            Save <Save />
          </IconButton>
        </DialogActions>
      </Dialog>
    </>
  );
}

export default SettingsDialog;

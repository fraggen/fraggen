import { FileUpload, Save } from '@mui/icons-material';
import { Dialog, DialogActions, DialogContent, DialogTitle, IconButton, TextField, } from '@mui/material';
import { ChangeEvent, useContext, useState } from 'react';
import { OpenDialogReturnValue } from 'electron';
import { LoadingContext } from '../../context/LoadingContext';
import { LogContext } from '../../context/LogContext';
import { ProjectContext } from '../../context/ProjectContext';
import { SnackbarContext } from '../../context/SnackbarContext';


interface CreateProjectDialogProps {
  open: boolean
  close: () => void
  openProjectTab: Function
}

function CreateProjectDialog({ open, close, openProjectTab }: CreateProjectDialogProps) {
  const logContext = useContext(LogContext);
  const loadingContext = useContext(LoadingContext);
  const projectContext = useContext(ProjectContext);
  const snackbarContext = useContext(SnackbarContext);

  const [name, setName] = useState('')
  const [description, setDescription] = useState('')
  const [projectFolder, setProjectFolder] = useState('')
  const [fps, setFps] = useState(60)
  // eslint-disable-next-line no-restricted-globals
  const [width, setWidth] = useState(screen.width)
  // eslint-disable-next-line no-restricted-globals
  const [height, setHeight] = useState(screen.height)

  async function createProject() {
    logContext.log('creating Project');
    loadingContext.setIsLoading(true);

    try {
      const project = await window.electron.createProject(name, projectFolder, fps, width, height, description)
      projectContext.setProject(project);
    } catch (error: any) {
      snackbarContext.addError(error.message);
      loadingContext.setIsLoading(false);
      return
    }

    logContext.log('created Project: ' + name);
    snackbarContext.addSuccess(`Created project: ${name}`);
    loadingContext.setIsLoading(false);
    openProjectTab();
    close()
  }


  function selectProjectFolder() {
    window.electron.selectDirectory().then((openDialogReturnValue: OpenDialogReturnValue) => { // todo snackbar alert
      if (!openDialogReturnValue.canceled) {
        const projectFolder = openDialogReturnValue.filePaths[0];
        setProjectFolder(projectFolder);
      }
    })
  }


  return (
    <Dialog
      open={open}
      maxWidth="sm"
      onClose={close}
      sx={{ minWidth: 275}}
    >
      <DialogTitle>Create a new project</DialogTitle>

      <DialogContent
        sx={{
          display: 'flex',
          flexDirection: 'column',
          m: 'auto',
          width: 'fit-content',
        }}
      >
        <TextField
          label="Name"
          id="name-input"
          margin="normal"
          variant="standard"
          value={name}
          onChange={(event: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => setName(event.target.value)}
        />
        <TextField
          label="Description"
          id="description-input"
          margin="normal"
          variant="standard"
          value={description}
          onChange={(event: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => setDescription(event.target.value)}
        />
        <TextField
          label="Project Folder"
          id="project-folder-input"
          margin="normal"
          variant="standard"
          value={projectFolder}
          onChange={(event: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => setProjectFolder(event.target.value)}
          InputProps={{endAdornment: (
            <IconButton color="primary" aria-label="upload picture" component="span" onClick={selectProjectFolder}>
              <FileUpload />
            </IconButton>
            )}}
        />
        <TextField
          label="FPS"
          id="fps-input"
          margin="normal"
          variant="standard"
          type="number"
          value={fps}
          onChange={(change: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => setFps(Number.parseInt(change.target.value, 10))}
        />
        <TextField
          label="Width"
          id="width-input"
          margin="normal"
          variant="standard"
          type="number"
          value={width}
          onChange={(change: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => setWidth(Number.parseInt(change.target.value, 10))}
        />
        <TextField
          label="Height"
          id="height-input"
          margin="normal"
          variant="standard"
          type="number"
          value={height}
          onChange={(change: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => setHeight(Number.parseInt(change.target.value, 10))}
        />
      </DialogContent>

      <DialogActions>
        <IconButton
          disabled={name.length === 0 || projectFolder.length === 0}
          onClick={createProject}
        >
          Save <Save />
        </IconButton>
      </DialogActions>
    </Dialog>
  );
}

export default CreateProjectDialog;

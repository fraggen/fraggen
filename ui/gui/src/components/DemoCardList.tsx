import { FraggenDemo } from '@fraggen/pipeline';
import { HideImage, Image, ViewColumn, ViewModule } from '@mui/icons-material';
import { Grid, IconButton, Paper, Tooltip, } from '@mui/material';
import { useContext } from 'react';
import { LoadingContext } from '../context/LoadingContext';
import { SettingsContext } from '../context/SettingsContext';
import DemoCard from './DemoCard';


function DemoCardList({ demos, selection, sceneSelection, showSceneTable = true  }: { demos: FraggenDemo[], selection?: boolean, sceneSelection?: boolean, showSceneTable?: boolean  }) {
  const loadingContext = useContext(LoadingContext);
  const settingsContext = useContext(SettingsContext);

  return (
    <Paper variant="outlined" sx={{display: 'flex', flexDirection: 'column', mt: '2em'}} >
      <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between', padding: '0 2em 0 1em'}}>
        <h4 style={{margin: '1em'}}>Demos</h4>
        <div style={{display: 'flex', flexDirection: 'row'}}>
          <Tooltip title={settingsContext.viewWide ? "View Compact" : "View Wide"}>
            <IconButton
              onClick={() => settingsContext.setViewWide(!settingsContext.viewWide)}
              disabled={loadingContext.isLoading}
            >
              {settingsContext.viewWide ? <ViewModule sx={{ transform: "rotate(90deg)" }} /> : <ViewColumn sx={{ transform: "rotate(90deg)" }} />}
            </IconButton>
          </Tooltip>
          <Tooltip title={settingsContext.showImages ? "Hide Map Images" : "Show Map Images"}>
            <IconButton
              onClick={() => settingsContext.setShowImages(!settingsContext.showImages)}
              disabled={loadingContext.isLoading}
            >
              {settingsContext.showImages ? <HideImage />: <Image />}
            </IconButton>
          </Tooltip>
        </div>
      </div>
      <Grid container sx={{padding: '1em 1em 0 1em'}}>
        {demos.map((demo: FraggenDemo, index: number) => (
          <Grid key={demo.file} item xs={settingsContext.viewWide ? 12 : 6}>
            <DemoCard demoIndex={index} demo={demo} showImage={settingsContext.showImages} showSceneTable={showSceneTable} selection={selection} sceneSelection={sceneSelection}/>
          </Grid>
        ))}
      </Grid>
      {demos.length === 0 && <div style={{ padding: '2em', textAlign: 'center' }}>
          Add Demos to start!
      </div>}
    </Paper>
);
}

export default DemoCardList;

import { FraggenDemo } from '@fraggen/pipeline';
import { Delete, Edit } from '@mui/icons-material';
import { Card, CardContent, CardHeader, CardMedia, Checkbox, IconButton, Tooltip, useTheme } from '@mui/material';
import { useContext } from 'react';
import { LoadingContext } from '../context/LoadingContext';
import { ProjectContext } from '../context/ProjectContext';
import SceneTable from './SceneTable';

interface DemoCardProps {
  demoIndex: number,
  demo: FraggenDemo,
  showImage?: boolean
  showSceneTable?: boolean
  selection?: boolean,
  sceneSelection?: boolean
}

function DemoCard({ demoIndex,  demo, showImage = true, showSceneTable = true, selection = false, sceneSelection = false }: DemoCardProps) {
  const loadingContext = useContext(LoadingContext);
  const projectContext = useContext(ProjectContext);

  function toggleChecked() {
    projectContext.project!.demos[demoIndex].checked = !demo.checked;
    projectContext.saveProject(projectContext.project!);// todo snackbar alert
  }

  const theme = useTheme();

  return (
    <Card elevation={6} sx={{ margin: "0 1em 2em 1em", backgroundColor: theme.palette.primary.light }}>
      <CardHeader
        title={
          <>
            {selection ?
              <Tooltip title={demo.checked ? "Exclude" : "Include"} >
                <Checkbox
                  checked={demo.checked}
                  onClick={toggleChecked}
                  inputProps={{ 'aria-label': 'controlled' }}
                  disabled={loadingContext.isLoading}
                />
              </Tooltip> : undefined
            }
            {demo.name}
            <Tooltip title="todo">
            <IconButton
              disabled={loadingContext.isLoading}
            >
              <Edit />
            </IconButton>
            </Tooltip>
            <Tooltip title="todo">
            <IconButton
              disabled={loadingContext.isLoading}
            >
              <Delete />
            </IconButton>
            </Tooltip>
          </>
        }
      />
      {showImage && <CardMedia
        component="img"
        height="194"
        image={ process.env.PUBLIC_URL + "/images/maps/"+ demo.map +".jpg" }
        alt="Use your imagination! Or open an issue in gitlab: https://gitlab.com/fraggen/fraggen/-/issues/new"
      />}
      <CardContent>
        <div>{demo.map}</div>
        <div>{demo.scenes.length} Scenes</div>
        {showSceneTable && <SceneTable demoIndex={demoIndex} scenes={demo.scenes} selection={sceneSelection} />}
      </CardContent>
    </Card>
  );
}

export default DemoCard;

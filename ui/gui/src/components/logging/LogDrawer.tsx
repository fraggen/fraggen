import { ArrowDownward, ArrowUpward } from '@mui/icons-material';
import { Box, CircularProgress, Drawer, IconButton } from '@mui/material';
import { grey } from '@mui/material/colors';
import { useContext } from 'react';
import { LogContext } from '../../context/LogContext';
import { MainLoggerState } from '../../types/MainLogger';
import WorkerLogger from '../../types/WorkerLogger';
import LogConsole from './LogConsole';
import ProgressStepper from './ProgressStepper';
import useResize from '../useResize';

const MIN_HEIGHT = 55;
const CLOSED_BIAS = 8; // almost closed drawer will be detected as closed
const DEFAULT_HEIGHT = 400;
const PULLER_HEIGHT = 10;

interface LogDrawerProps {
  openSceneExtractorTab: Function;
  openSceneRecorderTab: Function;
  openMovieMagicTab: Function;
}

function LogDrawer({openSceneExtractorTab, openSceneRecorderTab, openMovieMagicTab}: LogDrawerProps) {
  const logContext = useContext(LogContext);
  let { height, enableResize, setHeight } = useResize(MIN_HEIGHT);

  const open = height > MIN_HEIGHT + CLOSED_BIAS;
  const fullscreen = height === window.innerHeight;

  const toggleFullscreen = () => {
    setHeight(fullscreen ? DEFAULT_HEIGHT: window.innerHeight)
  }

  return (
    <>
      <div id="drawer-placeholder" style={{ height: MIN_HEIGHT}}/>
      <Drawer
        variant="permanent"
        anchor="bottom"
        open
        PaperProps={{ style: { height } }}
      >
        <div
          id="tab-puller"
          onMouseDown={enableResize}
          onDoubleClick={toggleFullscreen}
          style={{
            position: 'absolute',
            height: '0px',
            top: '-1px',
            right: '0',
            left: '0',
            cursor: 'row-resize',
            color:  grey[200],
            border: PULLER_HEIGHT/2 + 'px solid',
          }}
        />
        <div
          id="tab-puller-placeholder"
          style={{ height: PULLER_HEIGHT + 'px'}}
        />
        {logContext.mainLogger.state === MainLoggerState.WORKING ?
          <CircularProgress
            disableShrink
            size={"35px"}
            onDoubleClick={toggleFullscreen}
            sx={{ position: 'absolute', top: PULLER_HEIGHT-2, margin: '5px 10px 0 10px' }}
          /> : null
        }
        <div style={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-around',
          width: '100%'
        }}>
          <div id="circular-progress-placeholder" style={{ width: '53px'}}/>
          <ProgressStepper
            openSceneExtractorTab={openSceneExtractorTab}
            openSceneRecorderTab={openSceneRecorderTab}
            openMovieMagicTab={openMovieMagicTab}
          />
          <Box
            onDoubleClick={toggleFullscreen}
            sx={{ p: 1, backgroundColor: grey[100], flexGrow: 1, textAlign: 'center'}}
          >
            <IconButton
              color="primary"
              aria-label="toggle log drawer"
              component="span"
              onClick={() => setHeight(open ? MIN_HEIGHT: DEFAULT_HEIGHT)}
              sx={{backgroundColor: grey[200], fontSize: '16px'}}
            >
              Logs
              {open ? <ArrowDownward sx={{fontSize: '16px'}}/> : <ArrowUpward sx={{fontSize: '16px'}}/>}
            </IconButton>
          </Box>
        </div>
        {open && (
          <div  style={{ display: 'flex', flexDirection: 'row', overflow: 'hidden', height: '100%' }}>
            <LogConsole name={'Main'} key={'MainLogger'} log={logContext.mainLogger.log} state={logContext.mainLogger.state}/>
            {logContext.recorderLogger && <LogConsole name={'Recorder'} key={'RecorderLogger'} log={logContext.recorderLogger.log} state={logContext.recorderLogger.state}/>}
            {logContext.sandboxLoggers.map((sandboxLogger: WorkerLogger, index: number) => (
              <LogConsole log={sandboxLogger.log} name={'Sandbox ' + (index + 1)} key={'SandboxedRecorderLogger-' +  (index + 1)} state={sandboxLogger.state}/>
            ))}
          </div>
        )}
      </Drawer>
    </>
  );
}

export default LogDrawer;

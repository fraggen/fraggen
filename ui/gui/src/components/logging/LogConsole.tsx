import { Camera, CheckCircle, HourglassEmpty, StopCircle } from '@mui/icons-material';
import { Paper, Typography } from '@mui/material';
import { green, grey, orange, red } from '@mui/material/colors';
import { createRef, useEffect } from 'react';

function LogConsole({ log, name, state }: { log: string[], name: string, state: 'stopped' | 'starting' | 'ready' | 'recording' | 'idle' | 'working' }) {
  const scrollDivRef = createRef<HTMLDivElement>();

  useEffect(() => {
    if (scrollDivRef.current) {
      scrollDivRef.current.scrollTo(0, scrollDivRef.current.scrollHeight);
    }
  }, [log, scrollDivRef])

  function renderState() {
    if (!state) {
      return (<></>);
    }

    let style: any = {
      color: grey[300],
      margin: '2px',
      marginRight: '10px',
      display: 'flex',
      alignItems: 'center'
    };

    let icon

    switch (state) {
      case 'idle':
      case 'stopped':
        icon = <StopCircle />;
        break;
      case 'starting':
      case 'working':
        style.color = orange[300];
        icon = <HourglassEmpty />;
        break;
      case 'ready':
        style.color = green[300];
        icon = <CheckCircle />;
        break;
      case 'recording':
        style.color = red[300];
        icon = <Camera />;
        break;
    }

    return <div style={style} ><div style={{marginRight: '3px'}}>{state}</div>{icon}</div>
  }

  return (
    <Paper
      ref={scrollDivRef}
      sx={{
      textAlign: 'left',
      backgroundColor: 'black',
      color: 'white',
      overflowY: 'scroll',
      width: '100%',
      height: '100%',
      padding: '5px'
    }}
    >
      <div style={{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        position: 'sticky',
        top: '0',
        backgroundColor: 'darkgray'
      }}>
        <div style={{paddingLeft: '5px'}}>
          {name + ': '}
        </div>
        {renderState()}
      </div>
      <div style={{marginBottom: '10px'}}>
        {log.map((entry: string, index: number) => (
          <Typography key={index}>
            {entry} <br/>
          </Typography>
        ))}
      </div>
    </Paper>
  );
}

export default LogConsole;

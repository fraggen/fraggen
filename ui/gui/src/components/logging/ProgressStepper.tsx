import { Step, StepButton, Stepper } from '@mui/material';
import { useContext, useEffect, useState } from 'react';
import { LoadingContext } from '../../context/LoadingContext';
import { ProjectContext } from '../../context/ProjectContext';

const steps = ['Add Demos', 'Extract Scenes', 'Record Scenes', 'Generate Movie'];

interface StepperCompletion {
  [k: number]: boolean;
}

function ProgressStepper({openSceneExtractorTab, openSceneRecorderTab, openMovieMagicTab}: {openSceneExtractorTab: Function, openSceneRecorderTab: Function, openMovieMagicTab: Function}) {
  const loadingContext = useContext(LoadingContext);
  const projectContext = useContext(ProjectContext);

  const [activeStep, setActiveStep] = useState(0);
  const [completed, setCompleted] = useState<StepperCompletion>(computeCompleted());

  function computeCompleted(): StepperCompletion  {
    const newCompleted: StepperCompletion = {};

    if (projectContext.project && projectContext.project.demos.length > 0) {
      newCompleted[0] = true;
    }

    if (projectContext.getSceneCount() > 0) {
      newCompleted[1] = true;
    }

    if (projectContext.getRecordedSceneCount() > 0) {
      newCompleted[2] = true;
    }

    // todo when recorded & cut

    return newCompleted;
  }

  useEffect(()=> {
    setCompleted(computeCompleted());
  }, [projectContext.project])

  const handleStep = (step: number) => () => {
    setActiveStep(step);
    if (step === 0) {
      projectContext.addDemos()
    } else if (step === 1) {
      openSceneExtractorTab();
    } else if (step === 2) {
      openSceneRecorderTab();
    } else if (step === 3) {
      openMovieMagicTab();
    }
  };

  return (
    <Stepper nonLinear activeStep={activeStep} sx={{margin: '0 3em 0 3em', flexGrow: 2}}>
      {projectContext.project && steps.map((label, index) => (
        <Step key={label} completed={completed[index]} disabled={loadingContext.isLoading}>
          <StepButton color="inherit" onClick={handleStep(index)} sx={{paddingTop: 0, marginTop: 0, marginBottom: 0, paddingBottom: 0}}>
            {label}
          </StepButton>
        </Step>
      ))}
    </Stepper>
  );
}

export default ProgressStepper;

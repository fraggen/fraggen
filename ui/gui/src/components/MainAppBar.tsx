import { Add, FolderOpen, Settings } from '@mui/icons-material';
import { AppBar, Box, IconButton, Tab, Tabs, Tooltip, Typography, useTheme } from '@mui/material';
import { ReactNode, SyntheticEvent, useContext, useState } from 'react';
import { LoadingContext } from '../context/LoadingContext';
import { LogContext } from '../context/LogContext';
import { ProjectContext } from '../context/ProjectContext';
import LogLevel from '../types/LogLevel';
import CreateProjectDialog from './dialogs/CreateProjectDialog';
import SettingsDialog from './dialogs/SettingsDialog';
import LogDrawer from './logging/LogDrawer';
import MovieMagicTab from './tabs/MovieMagicTab';
import SceneExtractorTab from './tabs/SceneExtractorTab';
import SceneRecorderTab from './tabs/SceneRecorderTab';
import ProjectTab from './tabs/ProjectTab';
import StartTab from './tabs/StartTab';

interface TabPanelProps {
  children?: ReactNode;
  dir?: string;
  index: number;
  value: number;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography component={'div'}>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index: number) {
  return {
    id: `navigation-tab-${index}`,
    'aria-controls': `navigation-tab-panel-${index}`,
  };
}

const PROJECT_TAB = 0;
const SCENE_EXTRACTOR_TAB = 1;
const SCENE_RECORDER_TAB = 2;
const MOVIE_MAGIC_TAB = 3;
const TAB_NAMES = ['PROJECT_TAB', 'SCENE_EXTRACTOR_TAB', 'SCENE_RECORDER_TAB', 'MOVIE_MAGIC_TAB'];


function MainAppBar() {
  const theme = useTheme();
  const logContext = useContext(LogContext);
  const loadingContext = useContext(LoadingContext);
  const projectContext = useContext(ProjectContext);

  const [tabIndex, setTabIndex] = useState(PROJECT_TAB);
  const [showCreateProjectDialog, setShowCreateProjectDialog] = useState(false);
  const [showSettingsDialog, setShowSettingsDialog] = useState(false);

  const changeTab = (_event: SyntheticEvent, newValue: number) => {
    logContext.log("changeTab: " + TAB_NAMES[newValue], LogLevel.DEBUG)
    setTabIndex(newValue);
  };

  const openCreateProjectDialog = () => {
    logContext.log('openCreateProjectDialog', LogLevel.DEBUG);
    setShowCreateProjectDialog(true);
  };

  const closeCreateProjectDialog = () => {
    logContext.log('closeCreateProjectDialog', LogLevel.DEBUG);
    setShowCreateProjectDialog(false);
  };

  const openSettingsDialog = () => {
    logContext.log("openSettingsDialog", LogLevel.DEBUG)
    setShowSettingsDialog(true);
  };

  const closeSettingsDialog = () => {
    logContext.log("closeSettingsDialog", LogLevel.DEBUG)
    setShowSettingsDialog(false);
  };

  const appBarButtonStyle = {
    margin: '4px',
  }

 // todo extract InitPage component
  return (
    <div>
      <AppBar position="sticky" sx={{display: 'flex', flexDirection: 'row'}}>
        <Tooltip title="Create Project">
          <IconButton sx={appBarButtonStyle} onClick={openCreateProjectDialog} disabled={loadingContext.isLoading}><Add /></IconButton>
        </Tooltip>
        <Tooltip title="Open Project">
          <IconButton sx={appBarButtonStyle} onClick={projectContext.openProject} disabled={loadingContext.isLoading}><FolderOpen /></IconButton>
        </Tooltip>
        <Tabs
          value={tabIndex}
          onChange={changeTab}
          indicatorColor="secondary"
          textColor="inherit"
          aria-label="navigation-tabs"
          centered
          sx={{  display: 'flex', flexDirection: 'row', justifyContent: 'center', width: '100%' }}
        >
          <Tab sx={{  marginRight: 'auto' }} label={projectContext.project ? projectContext.project.name : "Project"} tabIndex={PROJECT_TAB} {...a11yProps(PROJECT_TAB)} disabled={loadingContext.isLoading} />
          <Tab label="Scene Extractor" tabIndex={SCENE_EXTRACTOR_TAB} {...a11yProps(SCENE_EXTRACTOR_TAB)} disabled={!projectContext.project || loadingContext.isLoading} />
          <Tab label="Scene Recorder" tabIndex={SCENE_RECORDER_TAB} {...a11yProps(SCENE_RECORDER_TAB)} disabled={!projectContext.project || loadingContext.isLoading} />
          <Tab sx={{  marginRight: 'auto' }} label="Movie Magic" tabIndex={MOVIE_MAGIC_TAB} {...a11yProps(MOVIE_MAGIC_TAB)} disabled={!projectContext.project || loadingContext.isLoading} />
        </Tabs>
        <Tooltip title="Settings">
          <IconButton sx={appBarButtonStyle} onClick={openSettingsDialog} disabled={loadingContext.isLoading}><Settings /></IconButton>
        </Tooltip>
      </AppBar>

      <TabPanel value={tabIndex} index={PROJECT_TAB} dir={theme.direction}>
        {projectContext.project ? <ProjectTab /> : <StartTab  openCreateProjectDialog={openCreateProjectDialog}/>}
      </TabPanel>

      <TabPanel value={tabIndex} index={SCENE_EXTRACTOR_TAB} dir={theme.direction}>
        <SceneExtractorTab />
      </TabPanel>

      <TabPanel value={tabIndex} index={SCENE_RECORDER_TAB} dir={theme.direction}>
        <SceneRecorderTab />
      </TabPanel>

      <TabPanel value={tabIndex} index={MOVIE_MAGIC_TAB} dir={theme.direction}>
        <MovieMagicTab />
      </TabPanel>

      <SettingsDialog
        open={showSettingsDialog}
        close={closeSettingsDialog}
      />
      <CreateProjectDialog
        open={showCreateProjectDialog}
        close={closeCreateProjectDialog}
        openProjectTab={() => changeTab({} as SyntheticEvent, PROJECT_TAB)}
      />
      <LogDrawer
        openSceneExtractorTab={() => changeTab({} as SyntheticEvent, SCENE_EXTRACTOR_TAB)}
        openSceneRecorderTab={() => changeTab({} as SyntheticEvent, SCENE_RECORDER_TAB)}
        openMovieMagicTab={() => changeTab({} as SyntheticEvent, MOVIE_MAGIC_TAB)}
      />
    </div>
  );
}

export default MainAppBar;

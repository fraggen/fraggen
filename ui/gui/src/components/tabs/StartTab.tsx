import { Add, FolderOpen } from '@mui/icons-material';
import { Button, } from '@mui/material';
import { useContext } from 'react';
import { LoadingContext } from '../../context/LoadingContext';
import { ProjectContext } from '../../context/ProjectContext';
import FraggenLogo from '../FraggenLogo';

function StartTab({openCreateProjectDialog}: {openCreateProjectDialog: () => void}) {
  const loadingContext = useContext(LoadingContext);
  const projectContext = useContext(ProjectContext);

  return (
    <div style={{ textAlign: "center" }}>
      <FraggenLogo />
      <Button
        size="large"
        variant="outlined"
        startIcon={<Add />}
        disabled={loadingContext.isLoading}
        onClick={openCreateProjectDialog}
        sx={{ m: 5, width: '225px', marginTop: '15em' }}
      >
        Create Project
      </Button>
      <div>
        <Button
          size="large"
          variant="outlined"
          startIcon={<FolderOpen />}
          disabled={loadingContext.isLoading}
          onClick={projectContext.openProject}
          sx={{ width: '225px' }}
        >
          Open Project
        </Button>
      </div>
    </div>
  );
}

export default StartTab;

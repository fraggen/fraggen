import { FraggenDemo, FraggenProject } from '@fraggen/pipeline';
import { Theaters } from '@mui/icons-material';
import { Button, Paper } from '@mui/material';
import { useContext } from 'react';
import { LoadingContext } from '../../context/LoadingContext';
import { LogContext } from '../../context/LogContext';
import { ProjectContext } from '../../context/ProjectContext';
import { SettingsContext } from '../../context/SettingsContext';
import { SnackbarContext } from '../../context/SnackbarContext';
import LogLevel from '../../types/LogLevel';
import DemoCardList from '../DemoCardList';
import SceneSelectorConfigurator from '../scene-selector-configurator/SceneSelectorConfigurator';

function SceneExtractorTab() {
  const logContext = useContext(LogContext);
  const loadingContext = useContext(LoadingContext);
  const projectContext = useContext(ProjectContext);
  const settingsContext = useContext(SettingsContext);
  const snackbarContext = useContext(SnackbarContext);

  function extractScenes() {
    logContext.log("extractScenes", LogLevel.DEBUG)
    loadingContext.setIsLoading(true);
    const sceneCountBefore = projectContext.project!.demos.reduce((count: number, demo: FraggenDemo) => count + demo.scenes.length, 0); // todo create getSceneCount in ProjectContext

    window.electron.getFreePort()
      .then((logPort: number) => {
        window.electron.extractScenes(settingsContext.sceneExtractorThreads, projectContext.project!, logPort)
          .then((project: FraggenProject) =>
            projectContext.saveProject(project)
              .then(() => {
                const newSceneCount = project.demos.reduce((count: number, demo: FraggenDemo) => count + demo.scenes.length, 0) - sceneCountBefore;
                if (newSceneCount > 0) {
                  snackbarContext.addSuccess(`${newSceneCount} scenes extracted.`);
                } else {
                  snackbarContext.addWarning('No scene found!');
                }
              })
          )
          .catch((err: Error) => snackbarContext.addError(err.message))
          .finally(() => loadingContext.setIsLoading(false));
        logContext.connectRecorderLogger(logPort);
      });
  }

  return (
    <Paper elevation={6} sx={{ padding: "2em", backgroundColor: "primary.light"}}>
      <div style={{display: 'flex', justifyContent: 'space-around', alignItems: 'center', marginBottom: '1em' }}>
        <SceneSelectorConfigurator />
        <Button
          variant="contained"
          onClick={extractScenes}
          endIcon={<Theaters />}
          disabled={!projectContext.project!.sceneSelectors || projectContext.project!.sceneSelectors?.length === 0 || loadingContext.isLoading}
          sx={{margin: '2em', height: '4em'}}
        >
          Extract Scenes
        </Button>
      </div>
      <DemoCardList demos={projectContext.project!.demos} selection />
    </Paper>
  );
}

export default SceneExtractorTab;

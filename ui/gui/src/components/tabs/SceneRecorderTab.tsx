import { FraggenProject } from '@fraggen/pipeline';
import { DynamicFeed, VideoCameraBack } from '@mui/icons-material';
import { Button, Paper } from '@mui/material';
import { PortFinderOptions } from 'portfinder';
import { useContext } from 'react';
import { LoadingContext } from '../../context/LoadingContext';
import { LogContext } from '../../context/LogContext';
import { ProjectContext } from '../../context/ProjectContext';
import { SettingsContext } from '../../context/SettingsContext';
import { SnackbarContext } from '../../context/SnackbarContext';
import DemoCardList from '../DemoCardList';

async function findPorts(amount: number): Promise<number[]> {
  const ports: number[] = [];
  let startPort = 0;

  for (let i = 0; i < amount; i++) {
    const options: PortFinderOptions = { startPort };

    await window.electron.getFreePort(options).then((port: number) => {
      ports.push(port);
      startPort = port + 1;
    })
  }

  return ports;
}

function SceneRecorderTab() {
  const logContext = useContext(LogContext)
  const loadingContext = useContext(LoadingContext);
  const projectContext = useContext(ProjectContext);
  const settingsContext = useContext(SettingsContext);
  const snackbarContext = useContext(SnackbarContext);


  function recordProject () {
    logContext.log('recordProject')
    loadingContext.setIsLoading(true);

    window.electron.getFreePort().then((recorderLogPort: number) => {
      window.electron.recordProject({
        csgoPath: settingsContext.csgoPath,
        fps: projectContext.project!.fps,
        height: projectContext.project!.height,
        width: projectContext.project!.width,
        hlaePath: settingsContext.hlaePath,
        steamPath: settingsContext.steamPath,
        telnetPort: 2121,
        recorderLogPort
      }, projectContext.project!)
        .then((project: FraggenProject) => projectContext.saveProject(project))
        .then(() => snackbarContext.addSuccess('Finished recording!'))
        .catch((err: Error) => snackbarContext.addError(err.message))
        .finally(() => loadingContext.setIsLoading(false));

      logContext.connectRecorderLogger(recorderLogPort);
    });
  }

  function recordProjectSandboxed () {
    logContext.log('recordProjectSandboxed')
    loadingContext.setIsLoading(true);

    findPorts(settingsContext.sandboxCount).then((recorderLogPorts: number[]) => {
      window.electron.recordProjectSandboxed({
        sandboxieOptions: {
          sandboxiePath: settingsContext.sandboxiePath,
          steamPath: settingsContext.steamPath,
          workerCount: settingsContext.sandboxCount,
        },
        csgoPath: settingsContext.csgoPath,
        fps: projectContext.project!.fps,
        height: projectContext.project!.height,
        width: projectContext.project!.width,
        hlaePath: settingsContext.hlaePath,
        steamPath: settingsContext.steamPath,
        telnetPort: 2121,
        recorderLogPorts
      },
        projectContext.project!
      )
        .then((project: FraggenProject) => projectContext.saveProject(project))
        .then(() => snackbarContext.addSuccess('Finished recording!'))
        .catch((err: Error) => snackbarContext.addError(err.message))
        .finally(() => loadingContext.setIsLoading(false));

      recorderLogPorts.forEach((recorderLogPort: number) => {
        logContext.connectSandboxLogger(recorderLogPort);
      });
    });
  }

  return (
    <Paper elevation={6} sx={{ padding: "2em", backgroundColor: "primary.light" }}>
      <div style={{ margin: '1em 0 3em', display: 'flex', justifyContent: 'space-around'}}>
        <Button variant="contained" onClick={recordProject} endIcon={<VideoCameraBack />} disabled={loadingContext.isLoading} >
          Record
        </Button>
        <Button variant="contained" onClick={recordProjectSandboxed} endIcon={<DynamicFeed />} disabled={loadingContext.isLoading} >
          Record Sandboxed
        </Button>
      </div>
      <DemoCardList demos={projectContext.project!.demos} selection sceneSelection />
    </Paper>
  );
}

export default SceneRecorderTab;

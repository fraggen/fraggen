import { Add, FolderOpen, Refresh } from '@mui/icons-material';
import { Button, Paper } from '@mui/material';
import { useContext } from 'react';
import { LoadingContext } from '../../context/LoadingContext';
import { LogContext } from '../../context/LogContext';
import { ProjectContext } from '../../context/ProjectContext';
import { SnackbarContext } from '../../context/SnackbarContext';
import LogLevel from '../../types/LogLevel';
import DemoCardList from '../DemoCardList';

function ProjectTab() {
  const logContext = useContext(LogContext);
  const loadingContext = useContext(LoadingContext);
  const projectContext = useContext(ProjectContext);
  const snackbarContext = useContext(SnackbarContext);
  const project = projectContext.project!;

  function showProjectFolder() {
    logContext.log("showProjectFolder", LogLevel.DEBUG)
    window.electron.showFolder(project.projectFolder + '\\fraggen-project.json')
      .catch((err: Error) => snackbarContext.addError(err.message));
  }

  return (
    <Paper elevation={6} sx={{ padding: "2em", backgroundColor: "primary.light" }}>
      <div style={{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around'
      }}>
        <div style={{
          display: 'flex',
          flexDirection: 'column',
          marginBottom: '20px'
        }}>
          <h2 style={{marginTop: '5px'}}>{project.name}</h2>
          {project.description && <div>{project.description}</div>}
          <div>{project.demos.length} Demos</div>
          <div>{projectContext.getSceneCount()} Scenes</div>
          <div>{projectContext.getRecordedSceneCount()} Recordings</div>
        </div>
        <div style={{
          display: 'flex',
          flexDirection: 'column',
          marginTop: '1em'
        }}>
          <Button
            variant="contained"
            onClick={projectContext.addDemos}
            startIcon={<Add />}
            disabled={loadingContext.isLoading}
            sx={{ justifyContent: 'start', margin: '5px'}}
          >
            Add Demos
          </Button>
          <Button
            variant="outlined"
            onClick={projectContext.reloadProject}
            startIcon={<Refresh />}
            disabled={loadingContext.isLoading}
            sx={{ justifyContent: 'start', margin: '5px'}}
          >
            Reload Project
          </Button>
          <Button
            variant="outlined"
            onClick={showProjectFolder}
            startIcon={<FolderOpen />}
            disabled={loadingContext.isLoading}
            sx={{ justifyContent: 'start', margin: '5px'}}
          >
            Open Project Folder
          </Button>
        </div>
      </div>
      <DemoCardList demos={project.demos} showSceneTable={false} />
    </Paper>
  );
}

export default ProjectTab;

import {
  KillByPlayerSelectorParams,
  KillByWeaponAndPlayerSelectorParams,
  KillByWeaponSelectorParams,
  SceneSelectorConfig,
  SceneSelectorParams,
  XKillsPerRoundByPlayerSelectorSelectorParams,
  XKillsPerRoundSelectorParams,
  ZoomInAndDelayAndKillSelectorParams,
  ZoomInAndKillSelectorParams,
  ZoomInByPlayerSelectorParams
} from '@fraggen/scene-extractor';
import { Add } from '@mui/icons-material';
import { Button, FormControl, InputLabel, MenuItem, Select, SelectChangeEvent } from '@mui/material';
import { useContext, useState } from 'react';
import { LoadingContext } from '../../context/LoadingContext';
import { LogContext } from '../../context/LogContext';
import { ProjectContext } from '../../context/ProjectContext';
import LogLevel from '../../types/LogLevel';
import SceneSelectorConfigView from './SceneSelectorConfigView';

function SceneSelectorConfigurator() {
  const logContext = useContext(LogContext);
  const loadingContext = useContext(LoadingContext);
  const projectContext = useContext(ProjectContext);

  const [sceneSelectorString, setSceneSelectorString] = useState('');

  function addSceneSelector() {
    logContext.log('addSceneSelector', LogLevel.DEBUG);
    loadingContext.setIsLoading(true);

    if (sceneSelectorString === "KillSelector") {
      const params: SceneSelectorParams = {
        eventLength: 1,
        introSeconds: 5,
        outroSeconds: 5
      }
      projectContext.project?.sceneSelectors?.push({ name: 'KillSelector', ...params })
    } else if (sceneSelectorString === "KillByPlayerSelector") {
      const params: KillByPlayerSelectorParams = {
        eventLength: 1,
        introSeconds: 5,
        outroSeconds: 5,
        steam64Id: ""
      }
      projectContext.project?.sceneSelectors?.push({ name: 'KillByPlayerSelector', ...params })
   } else if (sceneSelectorString === "KillByWeaponSelector") {
      const params: KillByWeaponSelectorParams = {
        eventLength: 1,
        introSeconds: 5,
        outroSeconds: 5,
        weapon: ""
      }
      projectContext.project?.sceneSelectors?.push({ name: 'KillByWeaponSelector', ...params })
    } else if (sceneSelectorString === "KillByWeaponAndPlayerSelector") {
      const params: KillByWeaponAndPlayerSelectorParams = {
        eventLength: 1,
        introSeconds: 5,
        outroSeconds: 5,
        weapon: "",
        steam64Id: ""
      }
      projectContext.project?.sceneSelectors?.push({ name: 'KillByWeaponAndPlayerSelector', ...params })
    } else if (sceneSelectorString === "XKillsPerRoundSelector") {
      const params: XKillsPerRoundSelectorParams = {
        killTarget: 5,
        eventLength: 1,
        introSeconds: 5,
        outroSeconds: 5
      }
      projectContext.project?.sceneSelectors?.push({ name: 'XKillsPerRoundSelector', ...params })
    } else if (sceneSelectorString === "XKillsPerRoundByPlayerSelector") {
      const params: XKillsPerRoundByPlayerSelectorSelectorParams = {
        killTarget: 5,
        steam64Id: '',
        eventLength: 1,
        introSeconds: 5,
        outroSeconds: 5
      }
      projectContext.project?.sceneSelectors?.push({ name: 'XKillsPerRoundByPlayerSelector', ...params })
    } else if (sceneSelectorString === "ZoomInSelector") {
      const params: SceneSelectorParams = {
        eventLength: 1,
        introSeconds: 5,
        outroSeconds: 5
      }
      projectContext.project?.sceneSelectors?.push({ name: 'ZoomInSelector', ...params })
    } else if (sceneSelectorString === "ZoomInByPlayerSelector") {
      const params: ZoomInByPlayerSelectorParams = {
        steam64Id: '',
        eventLength: 1,
        introSeconds: 5,
        outroSeconds: 5
      }
      projectContext.project?.sceneSelectors?.push({ name: 'ZoomInByPlayerSelector', ...params })
    } else if (sceneSelectorString === "ZoomInAndKillSelector") {
      const params: ZoomInAndKillSelectorParams = {
        killTimeoutSeconds: 0.5,
        eventLength: 1,
        introSeconds: 5,
        outroSeconds: 5
      }
      projectContext.project?.sceneSelectors?.push({ name: 'ZoomInAndKillSelector', ...params })
    } else if (sceneSelectorString === "ZoomInAndDelayAndKillSelector") {
      const params: ZoomInAndDelayAndKillSelectorParams = {
        delay: 0.5,
        killTimeoutSeconds: 1.5,
        eventLength: 1,
        introSeconds: 5,
        outroSeconds: 5
      }
      projectContext.project?.sceneSelectors?.push({ name: 'ZoomInAndDelayAndKillSelector', ...params })
    }

    projectContext.saveProject(projectContext.project!)
      .finally(() => loadingContext.setIsLoading(false));
}

  return (
    <>
      <div style={{display: 'flex', flexDirection: 'column', margin: '10px' }}>
        <FormControl sx={{ m: 1, minWidth: 180 }}>
          <InputLabel id="demo-simple-select-autowidth-label">Scene Selector</InputLabel>
          <Select
            labelId="demo-simple-select-autowidth-label"
            id="demo-simple-select-autowidth"
            value={sceneSelectorString}
            onChange={(event: SelectChangeEvent) => setSceneSelectorString(event.target.value)}
            autoWidth
            label="Age"
            disabled={loadingContext.isLoading}
          >
            <MenuItem value={""} />
            <MenuItem value={"KillSelector"}>Any Kill</MenuItem>
            <MenuItem value={"KillByPlayerSelector"}>Kill By Player</MenuItem>
            <MenuItem value={"KillByWeaponSelector"}>Kill By Weapon</MenuItem>
            <MenuItem value={"KillByWeaponAndPlayerSelector"}>Kill By Weapon And Player</MenuItem>
            <MenuItem value={"XKillsPerRoundSelector"}>X Kills Per Round</MenuItem>
            <MenuItem value={"XKillsPerRoundByPlayerSelector"}>X Kills Per Round By Player</MenuItem>
            <MenuItem value={"ZoomInSelector"}>Zoom In</MenuItem>
            <MenuItem value={"ZoomInByPlayerSelector"}>Zoom In By Player</MenuItem>
            <MenuItem value={"ZoomInAndKillSelector"}>Zoom In And Kill</MenuItem>
            <MenuItem value={"ZoomInAndDelayAndKillSelector"}>Zoom In And Delay And Kill</MenuItem>
          </Select>
        </FormControl>
        <Button
          variant="contained"
          onClick={addSceneSelector}
          startIcon={<Add />}
          disabled={sceneSelectorString === '' || loadingContext.isLoading}
        >
          Add Scene Selector
        </Button>
      </div>
      <div style={{display: 'flex', flexDirection: 'column', margin: '10px' }}>
        {projectContext.project?.sceneSelectors?.map((sceneSelectorConfig: SceneSelectorConfig, index: number) => <SceneSelectorConfigView key={index} sceneSelectorConfig={sceneSelectorConfig} index={index} />)}
      </div>
    </>
  );
}

export default SceneSelectorConfigurator;

import {
  SceneSelectorConfig,
} from '@fraggen/scene-extractor';
import { Delete } from '@mui/icons-material';
import { Checkbox, FormControlLabel, IconButton, TextField } from '@mui/material';
import { ChangeEvent, useContext } from 'react';
import { LoadingContext } from '../../context/LoadingContext';
import { LogContext } from '../../context/LogContext';
import { ProjectContext } from '../../context/ProjectContext';
import LogLevel from '../../types/LogLevel';

function SceneSelectorConfigView({sceneSelectorConfig, index}: {sceneSelectorConfig: SceneSelectorConfig, index: number}) {
  const logContext = useContext(LogContext);
  const loadingContext = useContext(LoadingContext);
  const projectContext = useContext(ProjectContext);

  // todo debounce
  function onChange(key: string, value: any) {
    projectContext.project!.sceneSelectors![index][key] = value;
    saveSceneSelectorConfig();
  }

  function saveSceneSelectorConfig() {
    logContext.log('saveSceneSelectorConfig', LogLevel.DEBUG);
    loadingContext.setIsLoading(true);
    projectContext.saveProject(projectContext.project!)
      .finally(() => loadingContext.setIsLoading(false));
  }

  function deleteSceneSelector() {
    logContext.log('saveSceneSelectorConfig', LogLevel.DEBUG);
    loadingContext.setIsLoading(true);
    projectContext.project?.sceneSelectors?.splice(index, 1)
    saveSceneSelectorConfig();
  }

  switch (sceneSelectorConfig.name) {
    default:
      return (
        <div style={{border: '1px solid lightgrey', marginBottom: '4px'}}>
          <div>
            {sceneSelectorConfig.name.replace(/([A-Z])/g, (match) => ` ${match}`).replace(/^./, (match) => match.toUpperCase()).trim()}
            <IconButton onClick={deleteSceneSelector} disabled={loadingContext.isLoading} >
              <Delete />
            </IconButton>
          </div>
          <div>
            {Object.entries(sceneSelectorConfig).sort().map(([key, value]: [string, any]) => {
              // eslint-disable-next-line array-callback-return
              if (key === 'name') return;

              if (typeof value === 'number') {
                return <TextField
                  key={sceneSelectorConfig.name + "-" + key}
                  label={key}
                  id={sceneSelectorConfig.name + "-" + key + "-input"}
                  margin="normal"
                  variant="standard"
                  type="number"
                  value={value}
                  onChange={(change: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) =>  onChange(key, Number.parseInt(change.target.value, 10))}
                  onBlur={saveSceneSelectorConfig}
                  sx={{ mx: 1, width: '80px' }}
                  disabled={loadingContext.isLoading}
                />
              } else if (typeof value === 'string') {
                return <TextField
                  key={sceneSelectorConfig.name + "-" + key}
                  label={key}
                  id={sceneSelectorConfig.name + "-" + key + "-input"}
                  margin="normal"
                  variant="standard"
                  type="text"
                  value={value}
                  onChange={(change: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => onChange(key, change.target.value)}
                  onBlur={saveSceneSelectorConfig}
                  disabled={loadingContext.isLoading}
                  sx={{ mx: 1, width: '120px' }}
                />
              } else if (typeof value === 'boolean') {
                return <FormControlLabel
                  label={key}
                  control={<Checkbox
                    key={sceneSelectorConfig.name + "-" + key}
                    id={sceneSelectorConfig.name + "-" + key + "-input"}
                    value={value}
                    onChange={(change: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => onChange(key, change.target.value)}
                    onBlur={saveSceneSelectorConfig}
                    disabled={loadingContext.isLoading}
                  />}
                />
              } else {
                return <div key={sceneSelectorConfig.name + "-" + key}>{key + ": " + value}</div>
              }
            }).filter(e => e)}
          </div>
        </div>
      );
  }
}

export default SceneSelectorConfigView;

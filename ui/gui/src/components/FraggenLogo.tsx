import { Movie } from '@mui/icons-material';
import { Typography } from '@mui/material';

function FraggenLogo() {
  return (
    <div style={{ display: 'flex', flexDirection: 'column', marginTop: "200px" }}>
      <Movie sx={{fontSize: '80px', marginLeft: 'auto', marginRight: 'auto', marginBottom: "50px"}}  />
      <Typography variant="h2" component="div" align="center">
        @fraggen
      </Typography>
    </div>
  );
}

export default FraggenLogo;

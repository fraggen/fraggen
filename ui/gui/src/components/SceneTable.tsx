import { FraggenScene } from '@fraggen/pipeline';
import { Add, Delete, MovieCreation } from '@mui/icons-material';
import { Checkbox, IconButton, Tooltip } from '@mui/material';
import {DataGrid, GridActionsCellItem, GridColDef, GridRowParams} from '@mui/x-data-grid';
import { useContext } from 'react';
import { LoadingContext } from '../context/LoadingContext';
import { LogContext } from '../context/LogContext';
import { ProjectContext } from '../context/ProjectContext';
import LogLevel from '../types/LogLevel';

/*
const allScenes = project.demos.reduce((scenes: FraggenScene[], demo: FraggenDemo) => {
    scenes.push(...demo.scenes);
    return scenes;
  } , [])
 */

function SceneTable({ demoIndex, scenes, selection }: { demoIndex: number, scenes: FraggenScene[], selection: boolean }) {
  const logContext= useContext(LogContext);
  const loadingContext = useContext(LoadingContext);
  const projectContext = useContext(ProjectContext);

  function addScene() { // todo snackbar alert
    const project = projectContext.project!;

    project.demos[demoIndex].scenes = [...scenes, {
      id: scenes.length + 1,
      sceneSelector: "manual",
      event: "",
      startTick: 0,
      eventTick: 0,
      endTick: 0,
      playerToSpec: "",
      length: 0,
      checked: true,
      recorded: false
    }];

    projectContext.saveProject(project);
  }

  function editScene(scene: FraggenScene) {
    logContext.log('editScene', LogLevel.DEBUG);
    loadingContext.setIsLoading(true);
    const project = projectContext.project!;
    project.demos[demoIndex].scenes = [...scenes.filter((_scene: FraggenScene) => _scene.id !== scene.id), scene].sort((a: FraggenScene, b: FraggenScene) => a.id - b.id);

    projectContext.saveProject(project)
      .then(() => loadingContext.setIsLoading(false));
    return scene;
  }

  function deleteScene(scene: FraggenScene) {
    logContext.log('deleteScene', LogLevel.DEBUG);
    loadingContext.setIsLoading(true);
    const project = projectContext.project!;

    project.demos[demoIndex].scenes = scenes.filter((_scene: FraggenScene) => _scene.id !== scene.id).map((_scene: FraggenScene, index: number) => {
      _scene.id = index + 1
      return _scene
    })

    projectContext.saveProject(project).then(() => loadingContext.setIsLoading(false));
  }

  function selectScene(scene: FraggenScene) {
    logContext.log('selectScene', LogLevel.DEBUG);
    loadingContext.setIsLoading(true);
    projectContext.project!.demos[demoIndex].scenes[scene.id - 1].checked = !scene.checked;
    projectContext.saveProject(projectContext.project!).then(() => loadingContext.setIsLoading(false));
  }

  const columns: GridColDef<FraggenScene, any, any>[]  = [
    { headerName: "ID", field: "id", width: 50 },
    { headerName: "Scene Selector", field: "sceneSelector", width: 150 },
    { headerName: "Player", field: "playerToSpec", width: 150, editable: true },
    { headerName: "Start Tick", field: "startTick", type: "number", width: 80, editable: true },
    { headerName: "Event Tick", field: "eventTick", type: "number", width: 80, editable: true },
    { headerName: "End Tick", field: "endTick", type: "number", width: 80, editable: true },
    { headerName: "Length", field: "length", type: "number", width: 80 },
    {
      field: "actions",
      type: "actions",
      width: 80,
      getActions: (row: GridRowParams<FraggenScene>) => [
        <GridActionsCellItem
          icon={<Delete />}
          label="Delete"
          onClick={() => deleteScene(row.row)}
          disabled={loadingContext.isLoading}
        />],
      renderHeader: () =>  <Tooltip title="Add Scene"><IconButton onClick={addScene} sx={{marginLeft: "auto"}} disabled={loadingContext.isLoading} ><Add /></IconButton></Tooltip>
    }
  ];

  if (selection) {
    columns.unshift({
      headerName: '',
      field: 'checked',
      width: 100,
      renderCell: (a: any) =>
        <>
          <Tooltip title={a.row.checked ? 'Exclude' : 'Include'}>
            <Checkbox
              checked={a.row.checked}
              onClick={() => selectScene(a.row)}
              inputProps={{ 'aria-label': 'controlled' }}
            />
          </Tooltip>
          {a.row.recorded && <Tooltip title="Recorded"><MovieCreation/></Tooltip>}
        </>
    });
  } else {
    columns.unshift({
      headerName: "",
      field: "recorded",
      width: 50,
      renderCell: (a: any) => {
        if (a.row.recorded) {
          return <Tooltip title="Recorded"><MovieCreation/></Tooltip>
        }
        return <></>;
     },
    });
  }

  return (
    <div style={{ height: 300, width: '100%' }}>
      <DataGrid
        rows={scenes}
        columns={columns}
        //checkboxSelection={selection}
        //onSelectionModelChange={selectScene}
        editMode="row"
        processRowUpdate={editScene}
      />
    </div>
  );
}

export default SceneTable;

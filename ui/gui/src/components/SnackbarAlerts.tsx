import { Alert, Snackbar, SnackbarOrigin } from '@mui/material';
import { useContext, useState } from 'react';
import { SnackbarContext } from '../context/SnackbarContext';

function SnackbarAlerts() {
  const snackbarContext = useContext(SnackbarContext);

  const [anyOpen, setAnyOpen] = useState(false);
  const [successSnackbar, setSuccessSnackbar] = useState<undefined | string>();
  const [infoSnackbar, setInfoSnackbar] = useState<undefined | string>();
  const [warningSnackbar, setWarningSnackbar] = useState<undefined | string>();
  const [errorSnackbar, setErrorSnackbar] = useState<undefined | string>();

  function checkQueues() {
    if (!anyOpen) {
      if (snackbarContext.successQueue.length > 0) {
        setAnyOpen(true);
        setSuccessSnackbar(snackbarContext.successQueue[0]);
      } else if (snackbarContext.infoQueue.length > 0) {
        setAnyOpen(true);
        setInfoSnackbar(snackbarContext.infoQueue[0]);
      } else if (snackbarContext.warningQueue.length > 0) {
        setAnyOpen(true);
        setWarningSnackbar(snackbarContext.warningQueue[0]);
      } else if (snackbarContext.errorQueue.length > 0) {
        setAnyOpen(true);
        setErrorSnackbar(snackbarContext.errorQueue[0]);
      }
    }
  }

  setInterval(checkQueues, 100)


  function closeSuccessSnackbar() {
    snackbarContext.successQueue.shift();
    setSuccessSnackbar(undefined);
    setAnyOpen(false);
  }

  function closeInfoSnackbar() {
    snackbarContext.infoQueue.shift();
    setInfoSnackbar(undefined);
    setAnyOpen(false);
  }

  function closeWarningSnackbar() {
    snackbarContext.warningQueue.shift();
    setWarningSnackbar(undefined);
    setAnyOpen(false);
  }

  function closeErrorSnackbar() {
    snackbarContext.errorQueue.shift();
    setErrorSnackbar(undefined);
    setAnyOpen(false);
  }

  const anchorOrigin: SnackbarOrigin = {
    horizontal: 'center',
    vertical: 'bottom',
  };

  const sx = { marginBottom: '4em' };

  return (
    <>
      <Snackbar
        open={successSnackbar !== undefined}
        autoHideDuration={4000}
        onClose={closeSuccessSnackbar}
        anchorOrigin={anchorOrigin}
        sx={sx}
      >
        <Alert onClose={closeSuccessSnackbar} severity="success" sx={{ width: '100%' }}>
          {successSnackbar}
        </Alert>
      </Snackbar>

      <Snackbar
        open={infoSnackbar !== undefined}
        autoHideDuration={5000}
        onClose={closeInfoSnackbar}
        anchorOrigin={anchorOrigin}
        sx={sx}
      >
        <Alert onClose={closeInfoSnackbar} severity="info" sx={{ width: '100%' }}>
          {infoSnackbar}
        </Alert>
      </Snackbar>

      <Snackbar
        open={warningSnackbar !== undefined}
        autoHideDuration={6000}
        onClose={closeWarningSnackbar}
        anchorOrigin={anchorOrigin}
        sx={sx}
      >
        <Alert onClose={closeSuccessSnackbar} severity="warning" sx={{ width: '100%' }}>
          {warningSnackbar}
        </Alert>
      </Snackbar>

      <Snackbar
        open={errorSnackbar !== undefined}
        autoHideDuration={null}
        onClose={closeErrorSnackbar}
        anchorOrigin={anchorOrigin}
        sx={sx}
      >
        <Alert onClose={closeErrorSnackbar} severity="error" sx={{ width: '100%' }}>
          {errorSnackbar}
        </Alert>
      </Snackbar>
    </>
  )
}

export default SnackbarAlerts;

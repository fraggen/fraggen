import { createTheme, ThemeProvider } from '@mui/material';
import MainAppBar from './components/MainAppBar';

const theme = createTheme({
  palette: {
    primary: {
      main: '#607d8b',
      light: '#8eacbb',
      dark: '#34515e',
      contrastText: '#000',
    },
    secondary: {
      main: '#4ccfe0',
      light: '#87ffff',
      dark: '#009eae',
      contrastText: '#000',
    },
  },

});

function App() {
  return (
    <ThemeProvider theme={theme}>
      <MainAppBar />
    </ThemeProvider>
  );
}

export default App;

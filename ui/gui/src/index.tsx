import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import SnackbarAlerts from './components/SnackbarAlerts';
import LoadingProvider from './context/LoadingContext';
import ProjectProvider from './context/ProjectContext';
import LogProvider from './context/LogContext';
import SettingsProvider from './context/SettingsContext';
import SnackbarProvider from './context/SnackbarContext';
import reportWebVitals from './reportWebVitals';
import './index.css';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <React.StrictMode>
    <LogProvider>
      <LoadingProvider>
        <SettingsProvider>
          <SnackbarProvider>
            <ProjectProvider>
              <App />
              <SnackbarAlerts />
            </ProjectProvider>
          </SnackbarProvider>
        </SettingsProvider>
      </LoadingProvider>
    </LogProvider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

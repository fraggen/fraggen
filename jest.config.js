/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  projects: ['<rootDir>/core/scene-extractor', '<rootDir>/core/scene-recorder'],
  globals: {
    'ts-jest': {
      tsconfig: 'tsconfig-base.json',
    },
  },
};
